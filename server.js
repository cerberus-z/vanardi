process.env.NODE_PATH = __dirname + '/app';

require('module').Module._initPaths();

const config = require('config')
    , express = require('express')
    , cluster = require('cluster')
    , path = require('path')
    , fs = require('fs')
    , dbConnection = require('db')
    , bodyParser = require('body-parser')
    , methodOverride = require('method-override')
    , error = require('middleware/errorhandler')
    , flashMiddleware = require('middleware/flash')
    , favicon = require('serve-favicon')
    , async = require('async')
    , logger = require('morgan')
    , helmet = require('helmet')
    , cookieParser = require('cookie-parser')
    , session	= require('express-session')
    , multer = require('multer')
    , crypto = require('crypto')
    , mime = require('mime')
    , flash = require('connect-flash-plus');

/**
 *  Run worker and keep server alive
 */
if (cluster.isMaster) {
    cluster.fork();
    cluster.on('exit', function(worker, code, signal) {
        cluster.fork();
    });
}

if (cluster.isWorker) {
    startApp();
}


function startApp()
{
    const app = express();
    const isDevelopment = app.get('env') == 'development';

    app.use(helmet());
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, POST');
        next();
    });

    if (isDevelopment) {
        app.use(logger('dev'));
    }

    // view engine setup
    app.engine('html', require('swig').renderFile);
    app.set('view engine', 'html');
    app.set('views', path.join(__dirname, '/app/views'));

    app.use(favicon(__dirname + '/app/assets/images/favicons/favicon.ico'));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(multer({
        storage: multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, __dirname + '/uploads')
            },
            filename: function (req, file, cb) {
                crypto.pseudoRandomBytes(16, function (err, raw) {
                    cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
                });
            }
        })
    }).any());
    app.use(methodOverride((req, res) => {
        if (req.body !== void 0 && typeof req.body === 'object' && '_method' in req.body) {
            let method = req.body._method;
            delete req.body._method;
            return method;
        }
    }));
    app.use(cookieParser(config.get('cookie:secret')));
    app.use(session(config.get('session')));
    app.use(flash());
    app.use(flashMiddleware);

    app.use('/assets', express.static(path.join(__dirname, 'dist')));
    app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
    app.use("/app/assets", express.static(path.join(__dirname, "app/assets")));

    require('routes')(app);

    //setup static pages
    require('./setup')();

    //client side routing
    app.get('*', (req, res) => {
        let clientSettings = config.get('clientSettings');
        if (isDevelopment) {
            clientSettings.host = `localhost:${config.get('port')}`;
        }
        res.render('public', {settings: clientSettings});
    });

    app.use(error.error404Catcher);
    app.use(error.logErrors);
    app.use(error.clientErrorHandler);
    app.use(error.errorHandler);
    // start server
    //-------------------------------
    const http = require('http').Server(app);
    async.series(
        {
            dbConnect:  callback => {
                dbConnection.once("open", callback);
            },
            startServer: callback => {
                http.listen(config.get("port"), callback);
            }
        },
        (err, results) => {
            if (err) {
                return console.log(results);
            }
            console.log("server running on port " + config.get("port"));

            process.on('SIGINT', () =>
            {
                dbConnection.close(() =>
                {
                    console.log("server closed on port " + config.get("port"));
                    process.exit(0);
                });
            });
        }
    );
}