(function() {
    'use strict';

    angular
        .module('app.routes', ['ui.router'])
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
            function($stateProvider, $urlRouterProvider, $locationProvider) {

                $urlRouterProvider.otherwise('/404.html');

                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: 'app/views/pages/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'vm',
                        title: 'Home'
                    })
                    .state('wines', {
                        url: '/wines',
                        templateUrl: 'app/views/pages/wines.html',
                        controller: 'WinesCtrl',
                        controllerAs: 'vm',
                        title: 'Our wines'
                    })
                    .state('tours', {
                        url: '/tours',
                        templateUrl: 'app/views/pages/tours.html',
                        controller: 'ToursCtrl',
                        controllerAs: 'vm',
                        title: 'Wine Tours'
                    })
                    .state('events', {
                        url: '/events',
                        templateUrl: 'app/views/pages/events.html',
                        controller: 'EventsCtrl',
                        controllerAs: 'vm',
                        title: 'Events'
                    })
                    .state('event', {
                        url: '/events/:id',
                        templateUrl: 'app/views/pages/event.html',
                        controller: 'EventCtrl',
                        controllerAs: 'vm',
                        title: 'Event'
                    })
                    .state('news', {
                        url: '/news',
                        templateUrl: 'app/views/pages/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm',
                        title: 'News'
                    })
                    .state('article', {
                        url: '/news/:id',
                        templateUrl: 'app/views/pages/article.html',
                        controller: 'ArticleCtrl',
                        controllerAs: 'vm',
                        title: 'Article'
                    })
                    .state('membership', {
                        url: '/membership',
                        templateUrl: 'app/views/pages/membership.html',
                        controller: 'MembershipCtrl',
                        controllerAs: 'vm',
                        title: 'Membership'
                    })
                    .state('cart', {
                        url: '/cart',
                        templateUrl: 'app/views/pages/cart.html',
                        controller: 'CartCtrl',
                        controllerAs: 'vm',
                        title: 'Cart'
                    })
                    .state('wine', {
                        url: '/wine/:id',
                        templateUrl: 'app/views/pages/wine.html',
                        controller: 'WineCtrl',
                        controllerAs: 'vm',
                        title: 'Wine'
                    })
                    .state('about', {
                        url: '/about',
                        templateUrl: 'app/views/pages/about.html',
                        controller: 'AboutCtrl',
                        controllerAs: 'vm',
                        title: 'About us'
                    })
                    .state('contact', {
                        url: '/contact',
                        templateUrl: 'app/views/pages/contact.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'vm',
                        title: 'Contact us'
                    })
                    .state('thanks', {
                        url: '/thanks.html',
                        templateUrl: 'app/views/thanks.html',
                        params: {
                            full_name: '',
                            address: '',
                            quantity: ''
                        },
                        title: 'Thanks'
                    })
                    .state('construction', {
                        url: '/construction',
                        templateUrl: 'app/views/construction.html',
                        title: 'Under Construction'
                    })
                    .state('404', {
                        url: '/404.html',
                        templateUrl: 'app/views/404.html',
                        controller: 'NotFountCtrl',
                        controllerAs: 'vm',
                        title: 'Not found'
                    })
                    .state('auth', {
                        url: '/auth',
                        templateUrl: 'app/views/pages/auth.html',
                        controller: 'AuthCtrl',
                        controllerAs: 'vm',
                        title: 'Auth'
                    });


                $locationProvider.html5Mode(true);
            }
        ]);
})();
