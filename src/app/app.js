(function () {
    'use strict';

    angular.element(function() {
        angular.bootstrap(document, ['vanApp']);
    });

    angular.module('vanApp',
        [
            'ngAnimate',
            'ui.router',
            'restangular',
            'app.routes',
            'ngMap',
            'app.controllers',
            'app.services',
            'app.directives',
            'app.ui',
            'ui.mask',
            'LocalStorageModule',
            'ngSanitize',
            'vcRecaptcha',
            'templates' //generated templateCache in dist
        ])
        .run(run)
        .config(config);

    /**
     * Setup app modules
     */
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.services', []);
    angular.module('app.ui', []);
    angular.module('templates', []);


    /**
     * Setup router permissions, cache and other things
     * @type {[*]}
     */
    run.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$location', 'HttpCacheService', 'RestInterceptor', 'CommonService', 'localStorageService'];

    function run ($rootScope, $state, $stateParams, $window, $location, HttpCacheService, RestInterceptor, CommonService, localStorageService) {
        //Run http cache
        HttpCacheService.init();
        //Catch server errors
        RestInterceptor.init();

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            $rootScope.$state.current = toState;
            $rootScope.pageTitle = toState.title;
        });

        //get app info
        $rootScope.settings = {};
        CommonService.getAppSettings()
            .then(function(response) {
                var settings = {};
                response.settings.forEach(function(item) {
                    settings[item.type] = item.amount;
                });
                $rootScope.settings = settings;
                $rootScope.contacts = response.contacts;
            });

        //check legalization
        $rootScope.isLegalized = localStorageService.cookie.get('isLegalized');
        $rootScope.confirmLegalization = function() {
            $rootScope.isLegalized = true;
            localStorageService.cookie.set('isLegalized', true);
        }

        /**
         * Listen routing for Google analytics
         */
        $rootScope.$on('$viewContentLoaded', function(event) {
            console.log($location.url(), $window.ga);
            $window.ga('send', 'pageview', { page: $location.url() });
        });
    }

    /**
     * Setup configs for libs and 3rd party extension
     * @type {[*]}
     */
    config.$inject = ['AppSettingsConstants', 'RestangularProvider', 'localStorageServiceProvider'];

    function config(AppSettingsConstants, RestangularProvider, localStorageServiceProvider) {
        var apiPath = '/api';

        RestangularProvider.setFullResponse(false);
        RestangularProvider.setBaseUrl(
            document.location.protocol === 'https:' ? 'https://' + AppSettingsConstants.host + apiPath : 'http://' + AppSettingsConstants.host + apiPath
        );

        localStorageServiceProvider
            .setStorageType('sessionStorage');
    }

})();
