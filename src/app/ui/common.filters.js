/**
 * Created by Riskey on 5/9/17.
 */
(function () {
    'use strict';

    angular.module('app.ui')
        .filter('hasFieldError', [function () {
            return function (form, field) {
                try {
                    if (form && typeof form[field] === 'undefined') {
                        return false;
                    }
                    if (!form[field].$dirty && !form.$submitted) {
                        return false;
                    }
                    return form[field].$invalid;
                } catch ($e) {}

            };
    }]);

    angular.module('app.ui')
        .filter('prettyNumber', [function () {
            return function (num) {
                return String(num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
            };
        }]);

    angular.module('app.ui')
        .filter('someSelected', [function () {
            return function (array) {
                if (!array) {
                    return false;
                }
                if (!Array.isArray(array)) {
                    return false;
                }
                return array.some(function (item) {
                    return item.selected;
                });
            };
        }]);

    angular.module('app.ui')
        .filter('toFixed', [function () {
            return function (number, count) {
                number = number || 0;
                count = count || 0;
                number = parseFloat(number);
                return number.toFixed(count);
            };
        }]);

    angular.module('app.ui')
        .filter('parseStringDate', function () {
            return function (date, seperator) {
                var exploded_date = date.split(' ');
                return (seperator === 'date') ? exploded_date[0] : exploded_date[1];
            };
        });

    angular.module('app.ui')
        .filter('hideSelected', function () {
            return function (items, hideSelected) {
                var filteredItems = [];
                if (!hideSelected) { return items; }
                else {
                    items.map (function (item) {
                        if (!item.selected) { filteredItems.push(item); }
                    });
                    return filteredItems;
                }
            };
        });

    angular.module('app.ui')
        .filter('filteredBy', function () {
            return function (items, prop, value, reverse) {
                var out = [];

                items.map (function (item) {
                    var filtered = item.data.filter(function (d) {
                        if (d.hasOwnProperty(prop)) {
                            return reverse ? d[prop] !== value : d[prop] == value;
                        }
                    });
                    if (filtered.length) {
                        out = out.concat(item);
                    }
                });

                return out;
            }
        });
})();