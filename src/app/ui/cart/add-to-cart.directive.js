(function () {
    'use strict';
    angular
        .module('app.directives')
        .directive('addToCart', addToCart);

    addToCart.$inject = ['CartService'];

    function addToCart(CartService) {


        function linkFn (scope, el) {

            scope.add = function() {
                var item = scope.presave();
                if (item) {
                    CartService.addItem(item);
                    scope.aftersave();
                }
            }

        }

        return {
            restrict: "E",
            scope: {
                presave: '&',
                aftersave: '&'
            },
            link: linkFn,
            template: '<button type="button" class="btn-black" ng-click="add()">ADD TO CART</button>'
        };
    }

})();