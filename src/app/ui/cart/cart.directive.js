(function () {
    'use strict';
    angular
        .module('app.directives')
        .directive('cart', cartDirective);

    function cartDirective() {

        CartCtrl.$inject = ['$rootScope', 'CartService'];

        function CartCtrl ($rootScope, CartService) {
            var vm = this;

            function ngOnInit() {
                vm.cartItems = CartService.getItemSync();
            }

            ngOnInit();

            vm.remove = function (index) {
                CartService.remove(index);
            };

            vm.updateQuantity = function(quantity, index) {
            if (!quantity) {
                quantity = 1;
            }
            CartService.updateItem(index, 'quantity', quantity);
        };
        
        }

        return {
            restrict: "E",
            scope: {},
            templateUrl: 'app/views/partials/minicart.html',
            controller: CartCtrl,
            controllerAs: 'vm'
        };
    }

})();