(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('WineCtrl', WineCtrl);

    WineCtrl.$inject = ['$stateParams','vcRecaptchaService',  'WinesService'];

    function WineCtrl($stateParams, vcRecaptchaService, WinesService) {
        var vm = this;
        vm.newFeedback = {};
        vm.wineStars = []; //save as array -> ng-repeat
        vm.captchaReady = false;

        function ngOnInit() {
            WinesService.getById($stateParams.id)
                .then(function(response) {
                    vm.wine = response.wine;
                    vm.wine.quantity = 1;
                    vm.newFeedback.rate = '5';
                    vm.wine.galleryitem = (vm.wine.gallery && vm.wine.gallery.length) ? vm.wine.gallery[0].file : '';
                    vm.galleryActivation = false;
                    if (!vm.wine.avatar) {
                        vm.setActiveAvatar(0);
                    }
                    calcWineStars();
                });
        }

        ngOnInit();

        vm.galleryactivate = function(){
            vm.galleryActivation = true;
        }
        vm.getNumber = function(num){
            let value = parseFloat(num);
            return new Array(value);
        }

        vm.gallerydeactivate = function(){
            vm.galleryActivation = false;
        }
        function calcWineStars() {
            var wineStars = 0;
            vm.wine.feedback.forEach(function (item) {
                wineStars += parseFloat(item.rate);
            });
            vm.wineStars = new Array(Math.ceil(wineStars / vm.wine.feedback.length) || 0);
        }

        vm.setActiveGallery = function(index){
            vm.wine.galleryitem = vm.wine.gallery[index].file;
        };

        vm.saveFeedback = function(feedback) {
            WinesService.postFeedback($stateParams.id, feedback)
                .then(function (res) {
                    vm.newFeedback = {};
                    swal({
                        title: "Success",
                        text: "Your feedback successfully sent to moderators",
                        type: "success",
                        closeOnConfirm: true
                    });
                }, function(err) {
                    //default handler
                })
                .finally(function () {
                    grecaptcha.reset();
                });
        };

        vm.isCaptchaChecked = function () {
            if (vm.captchaReady) {
                return grecaptcha.getResponse();
            }
        };

        vm.preSaveInCart = function (wine) {
            return {
                id: wine._id,
                price: wine.price,
                name: wine.name,
                quantity: wine.quantity,
                avatar: wine.avatar,
                type:  wine.type || 'wine'
            }
        };

        vm.afterSaveInCart = function () {
          //hook
        }
    }
})();
