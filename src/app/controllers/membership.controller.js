(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('MembershipCtrl', MembershipCtrl);

    MembershipCtrl.$inject = ['$rootScope', '$sce','$state', 'PagesService'];

    function MembershipCtrl($rootScope, $sce, $state, PagesService) {
        var vm = this;
        vm.member = {};
        vm.captchaReady = false;

        function ngOnInit() {
            PagesService.getMembership()
                .then(function (response) {
                    vm.content = response.data;
                    vm.content.description = $sce.trustAsHtml(vm.content.description);
                });

            var dateOpt = {
                autoClose: true,
                position: 'bottom left',
                dateFormat: 'dd/mm/yyyy',
                onSelect: function(date, d, inst) {
                    var field_name = $(inst.el).attr('name');
                    vm.member[field_name] = date;
                    $rootScope.$digest();
                }
            };
            $('.datepicker-here').datepicker(dateOpt);
        }
        ngOnInit();

        vm.saveFormInstance = function (form) {
            vm.form = form;
        };

        vm.isCaptchaChecked = function () {
            if (vm.captchaReady) {
                return grecaptcha.getResponse();
            }
        };

        vm.saveMember = function () {
            PagesService.addMembership(vm.member)
                .then(function(response) {
                    vm.member = {};
                    swal('Success', 'Successfully subscribed', 'success')
                        .then(function () {
                            $state.go('home');
                        });

                });
        }
    }
})();