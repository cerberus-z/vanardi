(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('EventCtrl', EventCtrl);

    EventCtrl.$inject = ['$sce', '$stateParams', 'EventsService'];

    function EventCtrl($sce, $stateParams, EventsService) {
        var vm = this;

        function ngOnInit() {
            EventsService.getById($stateParams.id)
                .then(function (response) {
                    vm.event = response.event;
                    vm.event.quantity = 1;
                    vm.event.description = $sce.trustAsHtml(vm.event.description);
                });
        }
        ngOnInit();

        vm.isUpcomingEvent = function () {
            if (!vm.event) {
                return false;
            }
            var eventDate = moment(vm.event.date, 'DD/MM/YYYY');
            return moment().isSameOrBefore(eventDate);
        };

        vm.preSaveInCart = function (event) {
            return {
                id: event._id,
                quantity: event.quantity,
                date: event.date,
                name: event.title,
                price: event.price,
                avatar: event.avatar,
                type: event.type || 'event'
            }
        };

        vm.afterSaveInCart = function () {
            //hook
        }
    }
})();
