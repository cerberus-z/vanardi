(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('WinesCtrl', WinesCtrl);

    WinesCtrl.$inject = ['WinesService'];

    function WinesCtrl(WinesService) {
        var vm = this;

        function ngOnInit() {
            WinesService.get()
                .then(function (response) {
                    vm.wines = response.wines;
                    vm.content = response.content;
                });
        }

        ngOnInit();
    }
})();