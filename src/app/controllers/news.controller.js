(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('NewsCtrl', NewsCtrl);

    NewsCtrl.$inject = ['NewsService'];

    function NewsCtrl(NewsService) {
        var vm = this;

        vm.news = [];

        function ngOnInit() {
            NewsService.get()
                .then(function(res) {
                    vm.news = res.news;
                    vm.news.forEach(function (item) {
                        item.formatedDate = moment(item.date, 'DD/MM/YYYY').format('D MMM');
                    })
                })
        }

        ngOnInit();
    }
})();