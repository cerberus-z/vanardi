(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('NotFountCtrl', NotFountCtrl);

    NotFountCtrl.$inject = ['$rootScope'];

    function NotFountCtrl($rootScope) {
        var vm = this;

        $rootScope.pageTitle = '404 Error';
        vm.errorMessage = 'Sorry, but the requested page doesn\'t exist !';
    }
})();