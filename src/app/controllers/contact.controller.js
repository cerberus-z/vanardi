(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ContactCtrl', ContactCtrl);

    ContactCtrl.$inject = ['$rootScope', 'NgMap', 'ContactService'];

    function ContactCtrl($rootScope, NgMap, ContactService) {
        var vm = this;
        var _subscribeModel = {
            first_name: '',
            last_name: '',
            email: ''
        };
        vm.maps = [];

        vm.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBQKYGPPvzJ8k-kUkw-CeLzzxrD5hz7Xf4';

        vm.style = [{
            featureType: "administrative",
            elementType: "geometry",
            stylers: [{
                visibility: "off"
            }]
        }, {
            featureType: "administrative.country",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.province",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.locality",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.neighborhood",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.land_parcel",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }];

        function ngOnInit() {
            vm.subscribeModel = Object.assign({}, _subscribeModel);

            NgMap.getMap('single_map').then(function(map) { vm.maps.push(map); });

            NgMap.getMap('multi_map').then(function(map) { vm.maps.push(map); });

            ContactService.get()
                .then(function(response) {
                    vm.faq = response.faq;
                    vm.stores = response.stores;
                    vm.selectedPointer = vm.stores[0];
                })
        }

        ngOnInit();

        vm.showPointerInfo = function (e, mapIndex, InfoWindowId, pointerId, data) {
            vm.maps[mapIndex].showInfoWindow(InfoWindowId, pointerId);
            vm.selectedPointer = data ? data : vm.selectedPointer;
        };

        vm.subscribe = function() {
            ContactService.subscribeToNews(vm.subscribeModel)
                .then(function(res) {
                    vm.subscribeModel = Object.assign({}, _subscribeModel);
                    swal({
                        title: "Success!",
                        text: "Congratulations. You Subscribed successfully",
                        type: "success"
                    });
                }, function(err) {
                    // console.log('Something going wrong!');
                })

        };
    }



})();
