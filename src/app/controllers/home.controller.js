(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['PagesService'];

    function HomeCtrl(PagesService) {
        var vm = this;


        function ngOnInit() {
            PagesService.getHome()
                .then(function(response) {
                    vm.content = response.content;
                    var events = response.events;
                    var eventDate;

                    vm.events = events.filter(function(item) {
                        eventDate = moment(item.date, 'DD/MM/YYYY');
                        if (moment().isSameOrBefore(eventDate)) {
                            item.formattedDate = eventDate.format('D MMM');
                            return item;
                        }
                    });

                    vm.wines = response.wines;
                })
        }

        ngOnInit();
    }
})();