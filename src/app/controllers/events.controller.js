(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('EventsCtrl', EventsCtrl);

    EventsCtrl.$inject = ['EventsService'];

    function EventsCtrl(EventsService) {
        var vm = this;
        vm.events = {
            current: [],
            past: []
        };

        function ngOnInit() {
            EventsService.get()
                .then(function(response) {
                    groupByDate(response.events);
                })
        }

        function groupByDate(events) {
            var eventDate;
            events.forEach(function (item) {
                eventDate = moment(item.date, 'DD/MM/YYYY');
                item.formattedDate = eventDate.format('D MMM');
                if (moment().isSameOrBefore(eventDate)) {
                    vm.events.current.push(item);
                } else {
                    vm.events.past.push(item);
                }
            });
        }

        ngOnInit();
    }
})();