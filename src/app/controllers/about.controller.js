(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('AboutCtrl', AboutCtrl);

    AboutCtrl.$inject = ['PagesService'];

    function AboutCtrl(PagesService) {
    	var vm = this;

    	function ngOnInit() {
            PagesService.getAbout()
                .then(function (res) {
                    vm.data = res.items;
                }, function (err) {})
        }

        ngOnInit();
    }
})();
