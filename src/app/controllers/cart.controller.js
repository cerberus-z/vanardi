(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('CartCtrl', CartCtrl);

    CartCtrl.$inject = ['$rootScope', 'CartService'];

    function CartCtrl($rootScope, CartService) {
        var vm = this;

        vm.order = {};
        vm.form = {};
        vm.currentStep = 1;
        vm.discount = 0;
        vm.isAgree = false;
        vm.captchaReady = false;

        function ngOnInit() {
            vm.cartItems = CartService.getItemSync();
        }
        ngOnInit();

        function _initUI() {
            $(function() {
                var dateOpt = {
                    dateFormat: 'dd/mm/yyyy',
                    autoClose: true,
                    position: 'bottom left',
                    onSelect: function(date, d, inst) {
                        var field_name = $(inst.el).attr('name');
                        vm.order[field_name] = date;
                        $rootScope.$digest();
                    },
                    minDate: new Date()
                };
                $('.datepicker-past-block').datepicker(dateOpt);

                vm.order.delivery_time = vm.order.delivery_time || moment().format('HH:mm');

                $('.timepicker').wickedpicker({
                    now: vm.order.delivery_time,
                    twentyFour: true, //Display 24 hour format, defaults to false
                    upArrow: 'wickedpicker__controls__control-up', //The up arrow class selector to use, for custom CSS
                    downArrow: 'wickedpicker__controls__control-down', //The down arrow class selector to use, for custom CSS
                    close: 'wickedpicker__close', //The close class selector to use, for custom CSS
                    hoverState: 'hover-state', //The hover state class to use, for custom CSS
                    title: 'Timepicker', //The Wickedpicker's title,
                    showSeconds: false, //Whether or not to show seconds,
                    timeSeparator: ' : ', // The string to put in between hours and minutes (and seconds)
                    secondsInterval: 1, //Change interval for seconds, defaults to 1,
                    minutesInterval: 5, //Change interval for minutes, defaults to 1
                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                    afterShow: null, //A function to be called after the Wickedpicker is closed/hidden
                    show: null, //A function to be called when the Wickedpicker is shown
                    clearable: false, //Make the picker's input clearable (has clickable "x")
                });
            })
        }

        function formIsValid () {
            return Object.keys(vm.form).length  && vm.form.$valid;
        }

        function setStep(stepN) {
            vm.currentStep = stepN;
        }

        function checkPromoCode() {
            CartService.checkPromoCode({
                email: vm.order.email,
                promo_code: vm.order.promo_code
            }).then(function(response) {
                vm.discount = $rootScope.settings['club-member-discount'];
                setStep(3);
            }, function () {
                vm.discount = 0;
            });

        }

        vm.saveFormInstance = function (form) {
            vm.form = form;
        };

        vm.changeStep = function(stepN) {
            if (!vm.cartItems.length) return false;
            if (stepN == 'next') stepN = vm.currentStep + 1;
            if (stepN == 'prev') stepN = vm.currentStep - 1;
            if (stepN == 2) _initUI();
            if (stepN == 3) {
                if (!formIsValid()) return false;
                if (vm.order.promo_code) {
                    checkPromoCode(vm.order.promo_code);
                    return false;
                } else {
                    vm.discount = 0;
                }
            }
            setStep(stepN);
        };

        vm.calcTotalPrice = function () {
            var sum = 0;
            vm.cartItems.forEach(function(item) {
                sum += +item.quantity * +item.price;
            });
            return Math.round(sum * (100 - vm.discount) / 100) || 0;
        };

        vm.wineExist = function () {
            return vm.cartItems.some(function(item) {
                return item.type === 'wine';
            })
        };

        vm.updateQuantity = function(quantity, index) {
            if (!quantity) {
                quantity = 1;
            }
            CartService.updateItem(index, 'quantity', quantity);
        };

        vm.remove = function (index) {
            CartService.remove(index);
        };

        vm.isCaptchaChecked = function() {
            if (vm.captchaReady) {
                return grecaptcha.getResponse() && vm.isAgree;
            }
        };

        vm.submit = function () {
            CartService.sendRequest(vm.order);
        }
    }
})();