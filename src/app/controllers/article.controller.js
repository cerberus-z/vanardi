(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ArticleCtrl', ArticleCtrl);

    ArticleCtrl.$inject = ['$sce', '$stateParams', 'NewsService'];

    function ArticleCtrl($sce, $stateParams, NewsService) {
        var vm = this;

        function ngOnInit() {
            NewsService.getById($stateParams.id)
                .then(function (response) {
                    vm.article = response.news;
                    // vm.event.description = $sce.trustAsHtml(vm.event.description);
                });
        }
        ngOnInit();
    }
})();