(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ToursCtrl', ToursCtrl);

    ToursCtrl.$inject = ['$rootScope', 'ToursService'];

    function ToursCtrl($rootScope, ToursService) {
        var vm = this;
        vm.activeTour = null;
        vm.tourType = 'public';

        function ngOnInit() {
            ToursService.get()
                .then(function(response) {
                    vm.content = response.content;
                    vm.tours = response.tours;

                    vm.setTour(vm.tours[0]);
                });

            vm.privateTour = {
                time: '',
                date: '',
                quantity: 1,
                get price () { return $rootScope.settings['private-tour-price'] }
            };

            $('.datepicker-here')
                .datepicker({
                    autoClose: true,
                    dateFormat: 'dd/mm/yyyy',
                    onSelect: function(date) {
                        vm.privateTour.date = date;
                        $rootScope.$digest();
                    },
                })
        }

        vm.setTour = function (tour) {
            vm.activeTour = tour;
            if (!vm.activeTour.quantity) {
                vm.activeTour.quantity = 1;
            }
        };

        vm.setTourType = function (type) {
            vm.tourType = type;
        };

        vm.preSaveInCart = function (tour) {
            var isPublicTour = tour.hasOwnProperty('_id');
            if (!isPublicTour  && (!tour.date || !tour.time)) {
                return false;
            }
            var outputObj = {
                quantity: tour.quantity,
                date: tour.date,
                imageUrl: 'https://www.fivestardays.com/content/img/product/main/vineyard-tour-tutored-wine-01092522.jpg',
                price: tour.price || 0
            };
            if (isPublicTour) {
                outputObj.id = tour._id;
                outputObj.name = 'Public tour ' + tour.start_time + '-' + tour.end_time;
                outputObj.type = 'tour';
                outputObj.start_time = tour.start_time;
                outputObj.end_time = tour.end_time;

            } else {
                outputObj.time = tour.time;
                outputObj.name = 'Private tour ' + tour.time;
                outputObj.type = 'private_tour';
            }
            outputObj.name += ', '+tour.date;
            return outputObj;
        };

        vm.afterSaveInCart = function () {
            //hook
        };
        ngOnInit();
    }
})();
