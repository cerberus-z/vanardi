(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('HttpCacheService', HttpCacheService);

    HttpCacheService.$inject = ['Restangular', '$cacheFactory'];

    function HttpCacheService(Restangular, $cacheFactory) {
        var service = {};
        var cache = $cacheFactory('http');

        function init () {
            Restangular.setDefaultHttpFields({cache: cache});
            Restangular.setResponseInterceptor(function (response, operation) {
                if (operation === 'put' || operation === 'post' || operation === 'remove') {
                    service.clear();
                }

                return response;
            })
        }

        function clear() {
            cache.removeAll();
        }

        service.clear = clear;
        service.init = init;

        return service;
    }
})();