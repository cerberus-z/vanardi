(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('CartService', CartService);

    CartService.$inject = ['$state', 'Restangular', 'localStorageService'];

    function CartService($state, Restangular, localStorageService) {
        var service = {};

        var _cartItems = localStorageService.get('order') || [];

        function addItem (data) {
            var i = -1, field = 'id';
            _cartItems.forEach(function(item, j) {
                if (item.hasOwnProperty('_id')) {
                    field = '_id'
                }
                if (item[field] === data[field]) { i = j; }
            });

            if (i > -1) {
                _cartItems[i].quantity += 1;
            } else {
                _cartItems.push(data);
            }

            localStorageService.set('order', _cartItems);
        }

        function getItemSync() {
            return _cartItems;
        }

        function remove(index) {
            _cartItems.splice(index, 1);
            localStorageService.set('order', _cartItems);
        }

        function _clear() {
            _cartItems = [];
            localStorageService.remove('order');
        }

        function updateItem(index, field, value) {
            _cartItems[index][field] = value;
            localStorageService.set('order', _cartItems);
        }

        function checkPromoCode (request) {
            return Restangular.all('/orders/checkPromo').customPOST(request)
        }

        function sendRequest(data) {
            var request = Object.assign({}, data);
            request.items = getItemSync();
            Restangular.all('/orders').customPOST(request)
                .then(function(res) {
                    var total = 0;
                    request.items.forEach(function(item) {
                        if (item.type == 'wine') total += 1;
                    });
                    _clear();
                    $state.go('thanks', {
                        full_name:request.full_name,
                        address:request.address,
                        quantity: total,
                    });
                })
        }

        service.addItem = addItem;
        service.getItemSync = getItemSync;
        service.remove = remove;
        service.updateItem = updateItem;
        service.checkPromoCode = checkPromoCode;
        service.sendRequest = sendRequest;

        return service;
    }
})();