/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('ToursService', ToursService);

    ToursService.$inject = ['Restangular'];

    function ToursService(Restangular) {
        var service = {};
        var _tours = [];

        var _toursRoute = Restangular.all('pages/wine-tour');

        function get() {
            return _toursRoute.customGET();
        }

        service.get = get;
        service.tours = function () { return _tours; };

        return service;
    }
})();