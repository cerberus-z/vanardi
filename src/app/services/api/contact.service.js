/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('ContactService', ContactService);

    ContactService.$inject = ['Restangular'];

    function ContactService(Restangular) {
        var service = {};

        var _contactRoute = Restangular.all('/pages/contact');

        function get() {
            return _contactRoute.customGET();
        }

        function subscribeToNews(data) {
            return _contactRoute.customPOST(data, 'subscribe');
        }

        service.get = get;
        service.subscribeToNews = subscribeToNews;

        return service;
    }
})();