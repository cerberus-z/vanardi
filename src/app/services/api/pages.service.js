/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('PagesService', PagesService);

    PagesService.$inject = ['Restangular'];

    function PagesService(Restangular) {

        var service = {};

        var _homeRoute = Restangular.all('/pages/home');
        var _aboutRoute = Restangular.all('/pages/about');
        var _membershipRoute = Restangular.all('/pages/club-membership');

        function getAbout() {
            return _aboutRoute.customGET();
        }

        function getHome() {
            return _homeRoute.customGET();
        }

        function getMembership() {
            return _membershipRoute.customGET();
        }

        function addMembership(data) {
            return Restangular.all('/club-membership').customPOST(data);
        }


        service.getAbout = getAbout;
        service.getHome = getHome;
        service.getMembership = getMembership;
        service.addMembership = addMembership;

        return service;
    }
})();