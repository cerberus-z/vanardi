/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('EventsService', EventsService);

    EventsService.$inject = ['Restangular'];

    function EventsService(Restangular) {
        var service = {};
        var _events = [];

        var _eventsRoute = Restangular.all('events');

        function get() {
            return _eventsRoute.customGET();
        }

        function getById(id) {
            return _eventsRoute.one(id).get();
        }

        service.get = get;
        service.getById = getById;
        service.events = function () { return _events; };

        return service;
    }
})();