/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('CommonService', CommonService);

    CommonService.$inject = ['Restangular'];

    function CommonService(Restangular) {
        var service = {};

        function getAppSettings() {
            return Restangular.all('settings').customGET();
        }

        service.getAppSettings = getAppSettings;

        return service;
    }
})();