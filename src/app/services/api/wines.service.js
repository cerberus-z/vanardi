/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('WinesService', WinesService);

    WinesService.$inject = ['Restangular'];

    function WinesService(Restangular) {
        var service = {};
        var _wines = [];

        var _winesRoute = Restangular.all('/pages/our-wines');

        function get() {
            return _winesRoute.customGET();
        }

        function getById(id) {
            return _winesRoute.one(id).get();
        }

        function postFeedback(id, data) {
            return Restangular.all('/products').one(id).customPOST(data)
        }

        service.get = get;
        service.getById = getById;
        service.postFeedback = postFeedback;
        service.wines = function () { return _wines; };

        return service;
    }
})();