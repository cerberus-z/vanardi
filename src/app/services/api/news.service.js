/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('NewsService', NewsService);

    NewsService.$inject = ['Restangular'];

    function NewsService(Restangular) {
        var service = {};
        var _news = [];

        var _newsRoute = Restangular.all('news');

        function get() {
            return _newsRoute.customGET();
        }

        function getById(id) {
            return _newsRoute.one(id).get();
        }

        service.get = get;
        service.getById = getById;
        service.news = function () { return _news; };

        return service;
    }
})();