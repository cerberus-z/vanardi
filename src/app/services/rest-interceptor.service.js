(function () {
    'use strict';
    angular
        .module('app.services')
        .service('RestInterceptor', RestInterceptor);

    RestInterceptor.$inject = ['$rootScope', '$http', '$state', '$timeout', 'Restangular', 'localStorageService'];

    function RestInterceptor($rootScope, $http, $state, $timeout, Restangular, localStorageService) {

        var checkInterval = 10000;
        var hasValidationError = false;

        function init() {
            _checkQueueLogs();

            _setRequestInterceptor();
            _setResponseInterceptor();
            _setErrorInterceptor();
        }

        /**
         * Check queue logs for sent to server
         * @private
         */
        function _checkQueueLogs() {
            var tickId = $timeout(function tick() {
                var errors = localStorageService.get('errors') || [];
                if (errors.length) {
                    _logErrorOnServer(errors, function () {
                        tickId = $timeout(tick, checkInterval)
                    });
                }
            }, checkInterval);
        }

        /**
         * redirect 404 page or sent error to backend
         * @private
         */
        function _setErrorInterceptor() {
            Restangular.setErrorInterceptor(function(response) {
                clearValidationError();
                $rootScope.processing = false;
                if (response.status === 400) { //validation error
                    hasValidationError = true;
                    setValidationError(response.data.errors);
                }
                else if (response.status === 404) {
                    $state.go('404');
                } else {
                    var errorInfo = 'no any info :(';
                    if (navigator) {
                        errorInfo = {
                            actionUrl: response.config.url,
                            browserName: navigator.vendor,
                            onLine: navigator.onLine,
                            cookieEnabled: navigator.cookieEnabled,
                            doNotTrack: navigator.doNotTrack,
                            platform: navigator.platform,
                            date: new Date()
                        }
                    }
                    _alertError(response.data ?  response.data.errors : null);
                    _logErrorOnServer(errorInfo);
                }

                return false;
            });
        }

        /**
         * @private
         */
        function _setRequestInterceptor() {
            Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
                headers['X-Requested-With'] = 'XMLHttpRequest';
                $rootScope.processing = true;
            })
        }

        /**
         * @private
         */
        function _setResponseInterceptor() {
            Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                $rootScope.processing = false;
                if (operation == 'post') {
                    clearValidationError();
                }
                return data;
            });
        }

        /**
         * Show errors with sweetalert 2
         * @param errors
         * @private
         */
        function _alertError(errors) {
            var messages = [];
            if (errors) {
                if (errors.length) {
                    errors.forEach(function(item) {
                        messages.push(item.message);
                    });
                }
            }
            swal({
                title: "Error",
                type: "error",
                text: messages.join(' ') || 'Something going wrong!',
                closeOnConfirm: true
            });
        }

        /**
         *
         * @param errorInfo: array | object
         * @param next: fn
         * @private
         */
        function _logErrorOnServer(errorInfo, next) {

            $http({
                method: 'POST',
                url: '/log',
                data: errorInfo
            }).then(function (res) {
                //@TODO: add page for success log

                var errors = localStorageService.get('errors') || [];
                if (errors.length) {
                    localStorageService.remove('errors');
                }

            }, function (err) {
                var errors = localStorageService.get('errors');
                if (!errors) {
                    errors = [];
                }
                if (Array.isArray(errorInfo)) {
                    errors = errorInfo;
                } else {
                    errors.push(errorInfo);
                }

                localStorageService.set('errors', errors);

            }).finally(next || angular.noop);
        }

        /**
         * Set validation error under inputs
         * @param err = Array of errors
         */
        function setValidationError(err) {
            if (!err || !err.length ) {
                return false;
            }

            var $el, $parent;

            err.forEach(function (item) {
                $el = $('[name='+item.params.field+']');
                $parent = $el.parent();
                if (item.params) {
                    $parent
                        .addClass('has-danger');

                    $('<div class="form-control-feedback text-left js-alert">'+item.message+'</div>').insertAfter($el);
                    $el
                        .addClass('form-control-danger');
                }
            })

        }

        function clearValidationError() {
            if (hasValidationError) {
                var $el = $('input, textarea');
                $el.removeClass('form-control-danger');
                $el.parent().removeClass('has-danger');
                $('.js-alert').remove();

                hasValidationError = false;
            }
        }

        this.init = init;
        this.setValidationError = setValidationError;
        this.clearValidationError = clearValidationError;
    }
})();