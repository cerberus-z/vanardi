(function () {
    'use strict';

    angular.element(function() {
        angular.bootstrap(document, ['vanApp']);
    });

    angular.module('vanApp',
        [
            'ngAnimate',
            'ui.router',
            'restangular',
            'app.routes',
            'ngMap',
            'app.controllers',
            'app.services',
            'app.directives',
            'app.ui',
            'ui.mask',
            'LocalStorageModule',
            'ngSanitize',
            'vcRecaptcha',
            'templates' //generated templateCache in dist
        ])
        .run(run)
        .config(config);

    /**
     * Setup app modules
     */
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.services', []);
    angular.module('app.ui', []);
    angular.module('templates', []);


    /**
     * Setup router permissions, cache and other things
     * @type {[*]}
     */
    run.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$location', 'HttpCacheService', 'RestInterceptor', 'CommonService', 'localStorageService'];

    function run ($rootScope, $state, $stateParams, $window, $location, HttpCacheService, RestInterceptor, CommonService, localStorageService) {
        //Run http cache
        HttpCacheService.init();
        //Catch server errors
        RestInterceptor.init();

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            $rootScope.$state.current = toState;
            $rootScope.pageTitle = toState.title;
        });

        //get app info
        $rootScope.settings = {};
        CommonService.getAppSettings()
            .then(function(response) {
                var settings = {};
                response.settings.forEach(function(item) {
                    settings[item.type] = item.amount;
                });
                $rootScope.settings = settings;
                $rootScope.contacts = response.contacts;
            });

        //check legalization
        $rootScope.isLegalized = localStorageService.cookie.get('isLegalized');
        $rootScope.confirmLegalization = function() {
            $rootScope.isLegalized = true;
            localStorageService.cookie.set('isLegalized', true);
        }

        /**
         * Listen routing for Google analytics
         */
        $rootScope.$on('$viewContentLoaded', function(event) {
            console.log($location.url(), $window.ga);
            $window.ga('send', 'pageview', { page: $location.url() });
        });
    }

    /**
     * Setup configs for libs and 3rd party extension
     * @type {[*]}
     */
    config.$inject = ['AppSettingsConstants', 'RestangularProvider', 'localStorageServiceProvider'];

    function config(AppSettingsConstants, RestangularProvider, localStorageServiceProvider) {
        var apiPath = '/api';

        RestangularProvider.setFullResponse(false);
        RestangularProvider.setBaseUrl(
            document.location.protocol === 'https:' ? 'https://' + AppSettingsConstants.host + apiPath : 'http://' + AppSettingsConstants.host + apiPath
        );

        localStorageServiceProvider
            .setStorageType('sessionStorage');
    }

})();

(function() {
    'use strict';

    angular
        .module('app.routes', ['ui.router'])
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
            function($stateProvider, $urlRouterProvider, $locationProvider) {

                $urlRouterProvider.otherwise('/404.html');

                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: 'app/views/pages/home.html',
                        controller: 'HomeCtrl',
                        controllerAs: 'vm',
                        title: 'Home'
                    })
                    .state('wines', {
                        url: '/wines',
                        templateUrl: 'app/views/pages/wines.html',
                        controller: 'WinesCtrl',
                        controllerAs: 'vm',
                        title: 'Our wines'
                    })
                    .state('tours', {
                        url: '/tours',
                        templateUrl: 'app/views/pages/tours.html',
                        controller: 'ToursCtrl',
                        controllerAs: 'vm',
                        title: 'Wine Tours'
                    })
                    .state('events', {
                        url: '/events',
                        templateUrl: 'app/views/pages/events.html',
                        controller: 'EventsCtrl',
                        controllerAs: 'vm',
                        title: 'Events'
                    })
                    .state('event', {
                        url: '/events/:id',
                        templateUrl: 'app/views/pages/event.html',
                        controller: 'EventCtrl',
                        controllerAs: 'vm',
                        title: 'Event'
                    })
                    .state('news', {
                        url: '/news',
                        templateUrl: 'app/views/pages/news.html',
                        controller: 'NewsCtrl',
                        controllerAs: 'vm',
                        title: 'News'
                    })
                    .state('article', {
                        url: '/news/:id',
                        templateUrl: 'app/views/pages/article.html',
                        controller: 'ArticleCtrl',
                        controllerAs: 'vm',
                        title: 'Article'
                    })
                    .state('membership', {
                        url: '/membership',
                        templateUrl: 'app/views/pages/membership.html',
                        controller: 'MembershipCtrl',
                        controllerAs: 'vm',
                        title: 'Membership'
                    })
                    .state('cart', {
                        url: '/cart',
                        templateUrl: 'app/views/pages/cart.html',
                        controller: 'CartCtrl',
                        controllerAs: 'vm',
                        title: 'Cart'
                    })
                    .state('wine', {
                        url: '/wine/:id',
                        templateUrl: 'app/views/pages/wine.html',
                        controller: 'WineCtrl',
                        controllerAs: 'vm',
                        title: 'Wine'
                    })
                    .state('about', {
                        url: '/about',
                        templateUrl: 'app/views/pages/about.html',
                        controller: 'AboutCtrl',
                        controllerAs: 'vm',
                        title: 'About us'
                    })
                    .state('contact', {
                        url: '/contact',
                        templateUrl: 'app/views/pages/contact.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'vm',
                        title: 'Contact us'
                    })
                    .state('thanks', {
                        url: '/thanks.html',
                        templateUrl: 'app/views/thanks.html',
                        params: {
                            full_name: '',
                            address: '',
                            quantity: ''
                        },
                        title: 'Thanks'
                    })
                    .state('construction', {
                        url: '/construction',
                        templateUrl: 'app/views/construction.html',
                        title: 'Under Construction'
                    })
                    .state('404', {
                        url: '/404.html',
                        templateUrl: 'app/views/404.html',
                        controller: 'NotFountCtrl',
                        controllerAs: 'vm',
                        title: 'Not found'
                    })
                    .state('auth', {
                        url: '/auth',
                        templateUrl: 'app/views/pages/auth.html',
                        controller: 'AuthCtrl',
                        controllerAs: 'vm',
                        title: 'Auth'
                    });


                $locationProvider.html5Mode(true);
            }
        ]);
})();

(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('NotFountCtrl', NotFountCtrl);

    NotFountCtrl.$inject = ['$rootScope'];

    function NotFountCtrl($rootScope) {
        var vm = this;

        $rootScope.pageTitle = '404 Error';
        vm.errorMessage = 'Sorry, but the requested page doesn\'t exist !';
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('AboutCtrl', AboutCtrl);

    AboutCtrl.$inject = ['PagesService'];

    function AboutCtrl(PagesService) {
    	var vm = this;

    	function ngOnInit() {
            PagesService.getAbout()
                .then(function (res) {
                    vm.data = res.items;
                }, function (err) {})
        }

        ngOnInit();
    }
})();

(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ArticleCtrl', ArticleCtrl);

    ArticleCtrl.$inject = ['$sce', '$stateParams', 'NewsService'];

    function ArticleCtrl($sce, $stateParams, NewsService) {
        var vm = this;

        function ngOnInit() {
            NewsService.getById($stateParams.id)
                .then(function (response) {
                    vm.article = response.news;
                    // vm.event.description = $sce.trustAsHtml(vm.event.description);
                });
        }
        ngOnInit();
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('CartCtrl', CartCtrl);

    CartCtrl.$inject = ['$rootScope', 'CartService'];

    function CartCtrl($rootScope, CartService) {
        var vm = this;

        vm.order = {};
        vm.form = {};
        vm.currentStep = 1;
        vm.discount = 0;
        vm.isAgree = false;
        vm.captchaReady = false;

        function ngOnInit() {
            vm.cartItems = CartService.getItemSync();
        }
        ngOnInit();

        function _initUI() {
            $(function() {
                var dateOpt = {
                    dateFormat: 'dd/mm/yyyy',
                    autoClose: true,
                    position: 'bottom left',
                    onSelect: function(date, d, inst) {
                        var field_name = $(inst.el).attr('name');
                        vm.order[field_name] = date;
                        $rootScope.$digest();
                    },
                    minDate: new Date()
                };
                $('.datepicker-past-block').datepicker(dateOpt);

                vm.order.delivery_time = vm.order.delivery_time || moment().format('HH:mm');

                $('.timepicker').wickedpicker({
                    now: vm.order.delivery_time,
                    twentyFour: true, //Display 24 hour format, defaults to false
                    upArrow: 'wickedpicker__controls__control-up', //The up arrow class selector to use, for custom CSS
                    downArrow: 'wickedpicker__controls__control-down', //The down arrow class selector to use, for custom CSS
                    close: 'wickedpicker__close', //The close class selector to use, for custom CSS
                    hoverState: 'hover-state', //The hover state class to use, for custom CSS
                    title: 'Timepicker', //The Wickedpicker's title,
                    showSeconds: false, //Whether or not to show seconds,
                    timeSeparator: ' : ', // The string to put in between hours and minutes (and seconds)
                    secondsInterval: 1, //Change interval for seconds, defaults to 1,
                    minutesInterval: 5, //Change interval for minutes, defaults to 1
                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                    afterShow: null, //A function to be called after the Wickedpicker is closed/hidden
                    show: null, //A function to be called when the Wickedpicker is shown
                    clearable: false, //Make the picker's input clearable (has clickable "x")
                });
            })
        }

        function formIsValid () {
            return Object.keys(vm.form).length  && vm.form.$valid;
        }

        function setStep(stepN) {
            vm.currentStep = stepN;
        }

        function checkPromoCode() {
            CartService.checkPromoCode({
                email: vm.order.email,
                promo_code: vm.order.promo_code
            }).then(function(response) {
                vm.discount = $rootScope.settings['club-member-discount'];
                setStep(3);
            }, function () {
                vm.discount = 0;
            });

        }

        vm.saveFormInstance = function (form) {
            vm.form = form;
        };

        vm.changeStep = function(stepN) {
            if (!vm.cartItems.length) return false;
            if (stepN == 'next') stepN = vm.currentStep + 1;
            if (stepN == 'prev') stepN = vm.currentStep - 1;
            if (stepN == 2) _initUI();
            if (stepN == 3) {
                if (!formIsValid()) return false;
                if (vm.order.promo_code) {
                    checkPromoCode(vm.order.promo_code);
                    return false;
                } else {
                    vm.discount = 0;
                }
            }
            setStep(stepN);
        };

        vm.calcTotalPrice = function () {
            var sum = 0;
            vm.cartItems.forEach(function(item) {
                sum += +item.quantity * +item.price;
            });
            return Math.round(sum * (100 - vm.discount) / 100) || 0;
        };

        vm.wineExist = function () {
            return vm.cartItems.some(function(item) {
                return item.type === 'wine';
            })
        };

        vm.updateQuantity = function(quantity, index) {
            if (!quantity) {
                quantity = 1;
            }
            CartService.updateItem(index, 'quantity', quantity);
        };

        vm.remove = function (index) {
            CartService.remove(index);
        };

        vm.isCaptchaChecked = function() {
            if (vm.captchaReady) {
                return grecaptcha.getResponse() && vm.isAgree;
            }
        };

        vm.submit = function () {
            CartService.sendRequest(vm.order);
        }
    }
})();
(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ContactCtrl', ContactCtrl);

    ContactCtrl.$inject = ['$rootScope', 'NgMap', 'ContactService'];

    function ContactCtrl($rootScope, NgMap, ContactService) {
        var vm = this;
        var _subscribeModel = {
            first_name: '',
            last_name: '',
            email: ''
        };
        vm.maps = [];

        vm.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBQKYGPPvzJ8k-kUkw-CeLzzxrD5hz7Xf4';

        vm.style = [{
            featureType: "administrative",
            elementType: "geometry",
            stylers: [{
                visibility: "off"
            }]
        }, {
            featureType: "administrative.country",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.province",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.locality",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.neighborhood",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }, {
            featureType: "administrative.land_parcel",
            elementType: "geometry.stroke",
            stylers: [{
                visibility: "on"
            }]
        }];

        function ngOnInit() {
            vm.subscribeModel = Object.assign({}, _subscribeModel);

            NgMap.getMap('single_map').then(function(map) { vm.maps.push(map); });

            NgMap.getMap('multi_map').then(function(map) { vm.maps.push(map); });

            ContactService.get()
                .then(function(response) {
                    vm.faq = response.faq;
                    vm.stores = response.stores;
                    vm.selectedPointer = vm.stores[0];
                })
        }

        ngOnInit();

        vm.showPointerInfo = function (e, mapIndex, InfoWindowId, pointerId, data) {
            vm.maps[mapIndex].showInfoWindow(InfoWindowId, pointerId);
            vm.selectedPointer = data ? data : vm.selectedPointer;
        };

        vm.subscribe = function() {
            ContactService.subscribeToNews(vm.subscribeModel)
                .then(function(res) {
                    vm.subscribeModel = Object.assign({}, _subscribeModel);
                    swal({
                        title: "Success!",
                        text: "Congratulations. You Subscribed successfully",
                        type: "success"
                    });
                }, function(err) {
                    // console.log('Something going wrong!');
                })

        };
    }



})();

(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('EventCtrl', EventCtrl);

    EventCtrl.$inject = ['$sce', '$stateParams', 'EventsService'];

    function EventCtrl($sce, $stateParams, EventsService) {
        var vm = this;

        function ngOnInit() {
            EventsService.getById($stateParams.id)
                .then(function (response) {
                    vm.event = response.event;
                    vm.event.quantity = 1;
                    vm.event.description = $sce.trustAsHtml(vm.event.description);
                });
        }
        ngOnInit();

        vm.isUpcomingEvent = function () {
            if (!vm.event) {
                return false;
            }
            var eventDate = moment(vm.event.date, 'DD/MM/YYYY');
            return moment().isSameOrBefore(eventDate);
        };

        vm.preSaveInCart = function (event) {
            return {
                id: event._id,
                quantity: event.quantity,
                date: event.date,
                name: event.title,
                price: event.price,
                avatar: event.avatar,
                type: event.type || 'event'
            }
        };

        vm.afterSaveInCart = function () {
            //hook
        }
    }
})();

(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('EventsCtrl', EventsCtrl);

    EventsCtrl.$inject = ['EventsService'];

    function EventsCtrl(EventsService) {
        var vm = this;
        vm.events = {
            current: [],
            past: []
        };

        function ngOnInit() {
            EventsService.get()
                .then(function(response) {
                    groupByDate(response.events);
                })
        }

        function groupByDate(events) {
            var eventDate;
            events.forEach(function (item) {
                eventDate = moment(item.date, 'DD/MM/YYYY');
                item.formattedDate = eventDate.format('D MMM');
                if (moment().isSameOrBefore(eventDate)) {
                    vm.events.current.push(item);
                } else {
                    vm.events.past.push(item);
                }
            });
        }

        ngOnInit();
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['PagesService'];

    function HomeCtrl(PagesService) {
        var vm = this;


        function ngOnInit() {
            PagesService.getHome()
                .then(function(response) {
                    vm.content = response.content;
                    var events = response.events;
                    var eventDate;

                    vm.events = events.filter(function(item) {
                        eventDate = moment(item.date, 'DD/MM/YYYY');
                        if (moment().isSameOrBefore(eventDate)) {
                            item.formattedDate = eventDate.format('D MMM');
                            return item;
                        }
                    });

                    vm.wines = response.wines;
                })
        }

        ngOnInit();
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('MainCtrl', MainCtrl);

    function MainCtrl() {
        var vm = this;
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('MembershipCtrl', MembershipCtrl);

    MembershipCtrl.$inject = ['$rootScope', '$sce','$state', 'PagesService'];

    function MembershipCtrl($rootScope, $sce, $state, PagesService) {
        var vm = this;
        vm.member = {};
        vm.captchaReady = false;

        function ngOnInit() {
            PagesService.getMembership()
                .then(function (response) {
                    vm.content = response.data;
                    vm.content.description = $sce.trustAsHtml(vm.content.description);
                });

            var dateOpt = {
                autoClose: true,
                position: 'bottom left',
                dateFormat: 'dd/mm/yyyy',
                onSelect: function(date, d, inst) {
                    var field_name = $(inst.el).attr('name');
                    vm.member[field_name] = date;
                    $rootScope.$digest();
                }
            };
            $('.datepicker-here').datepicker(dateOpt);
        }
        ngOnInit();

        vm.saveFormInstance = function (form) {
            vm.form = form;
        };

        vm.isCaptchaChecked = function () {
            if (vm.captchaReady) {
                return grecaptcha.getResponse();
            }
        };

        vm.saveMember = function () {
            PagesService.addMembership(vm.member)
                .then(function(response) {
                    vm.member = {};
                    swal('Success', 'Successfully subscribed', 'success')
                        .then(function () {
                            $state.go('home');
                        });

                });
        }
    }
})();
(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('NewsCtrl', NewsCtrl);

    NewsCtrl.$inject = ['NewsService'];

    function NewsCtrl(NewsService) {
        var vm = this;

        vm.news = [];

        function ngOnInit() {
            NewsService.get()
                .then(function(res) {
                    vm.news = res.news;
                    vm.news.forEach(function (item) {
                        item.formatedDate = moment(item.date, 'DD/MM/YYYY').format('D MMM');
                    })
                })
        }

        ngOnInit();
    }
})();
(function() {
    'use strict';
    angular
        .module('app.controllers')
        .controller('ToursCtrl', ToursCtrl);

    ToursCtrl.$inject = ['$rootScope', 'ToursService'];

    function ToursCtrl($rootScope, ToursService) {
        var vm = this;
        vm.activeTour = null;
        vm.tourType = 'public';

        function ngOnInit() {
            ToursService.get()
                .then(function(response) {
                    vm.content = response.content;
                    vm.tours = response.tours;

                    vm.setTour(vm.tours[0]);
                });

            vm.privateTour = {
                time: '',
                date: '',
                quantity: 1,
                get price () { return $rootScope.settings['private-tour-price'] }
            };

            $('.datepicker-here')
                .datepicker({
                    autoClose: true,
                    dateFormat: 'dd/mm/yyyy',
                    onSelect: function(date) {
                        vm.privateTour.date = date;
                        $rootScope.$digest();
                    },
                })
        }

        vm.setTour = function (tour) {
            vm.activeTour = tour;
            if (!vm.activeTour.quantity) {
                vm.activeTour.quantity = 1;
            }
        };

        vm.setTourType = function (type) {
            vm.tourType = type;
        };

        vm.preSaveInCart = function (tour) {
            var isPublicTour = tour.hasOwnProperty('_id');
            if (!isPublicTour  && (!tour.date || !tour.time)) {
                return false;
            }
            var outputObj = {
                quantity: tour.quantity,
                date: tour.date,
                imageUrl: 'https://www.fivestardays.com/content/img/product/main/vineyard-tour-tutored-wine-01092522.jpg',
                price: tour.price || 0
            };
            if (isPublicTour) {
                outputObj.id = tour._id;
                outputObj.name = 'Public tour ' + tour.start_time + '-' + tour.end_time;
                outputObj.type = 'tour';
                outputObj.start_time = tour.start_time;
                outputObj.end_time = tour.end_time;

            } else {
                outputObj.time = tour.time;
                outputObj.name = 'Private tour ' + tour.time;
                outputObj.type = 'private_tour';
            }
            outputObj.name += ', '+tour.date;
            return outputObj;
        };

        vm.afterSaveInCart = function () {
            //hook
        };
        ngOnInit();
    }
})();

(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('WineCtrl', WineCtrl);

    WineCtrl.$inject = ['$stateParams','vcRecaptchaService',  'WinesService'];

    function WineCtrl($stateParams, vcRecaptchaService, WinesService) {
        var vm = this;
        vm.newFeedback = {};
        vm.wineStars = []; //save as array -> ng-repeat
        vm.captchaReady = false;

        function ngOnInit() {
            WinesService.getById($stateParams.id)
                .then(function(response) {
                    vm.wine = response.wine;
                    vm.wine.quantity = 1;
                    vm.newFeedback.rate = '5';
                    vm.wine.galleryitem = (vm.wine.gallery && vm.wine.gallery.length) ? vm.wine.gallery[0].file : '';
                    vm.galleryActivation = false;
                    if (!vm.wine.avatar) {
                        vm.setActiveAvatar(0);
                    }
                    calcWineStars();
                });
        }

        ngOnInit();

        vm.galleryactivate = function(){
            vm.galleryActivation = true;
        }
        vm.getNumber = function(num){
            let value = parseFloat(num);
            return new Array(value);
        }

        vm.gallerydeactivate = function(){
            vm.galleryActivation = false;
        }
        function calcWineStars() {
            var wineStars = 0;
            vm.wine.feedback.forEach(function (item) {
                wineStars += parseFloat(item.rate);
            });
            vm.wineStars = new Array(Math.ceil(wineStars / vm.wine.feedback.length) || 0);
        }

        vm.setActiveGallery = function(index){
            vm.wine.galleryitem = vm.wine.gallery[index].file;
        };

        vm.saveFeedback = function(feedback) {
            WinesService.postFeedback($stateParams.id, feedback)
                .then(function (res) {
                    vm.newFeedback = {};
                    swal({
                        title: "Success",
                        text: "Your feedback successfully sent to moderators",
                        type: "success",
                        closeOnConfirm: true
                    });
                }, function(err) {
                    //default handler
                })
                .finally(function () {
                    grecaptcha.reset();
                });
        };

        vm.isCaptchaChecked = function () {
            if (vm.captchaReady) {
                return grecaptcha.getResponse();
            }
        };

        vm.preSaveInCart = function (wine) {
            return {
                id: wine._id,
                price: wine.price,
                name: wine.name,
                quantity: wine.quantity,
                avatar: wine.avatar,
                type:  wine.type || 'wine'
            }
        };

        vm.afterSaveInCart = function () {
          //hook
        }
    }
})();

(function () {
    'use strict';
    angular
        .module('app.controllers')
        .controller('WinesCtrl', WinesCtrl);

    WinesCtrl.$inject = ['WinesService'];

    function WinesCtrl(WinesService) {
        var vm = this;

        function ngOnInit() {
            WinesService.get()
                .then(function (response) {
                    vm.wines = response.wines;
                    vm.content = response.content;
                });
        }

        ngOnInit();
    }
})();
(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('CartService', CartService);

    CartService.$inject = ['$state', 'Restangular', 'localStorageService'];

    function CartService($state, Restangular, localStorageService) {
        var service = {};

        var _cartItems = localStorageService.get('order') || [];

        function addItem (data) {
            var i = -1, field = 'id';
            _cartItems.forEach(function(item, j) {
                if (item.hasOwnProperty('_id')) {
                    field = '_id'
                }
                if (item[field] === data[field]) { i = j; }
            });

            if (i > -1) {
                _cartItems[i].quantity += 1;
            } else {
                _cartItems.push(data);
            }

            localStorageService.set('order', _cartItems);
        }

        function getItemSync() {
            return _cartItems;
        }

        function remove(index) {
            _cartItems.splice(index, 1);
            localStorageService.set('order', _cartItems);
        }

        function _clear() {
            _cartItems = [];
            localStorageService.remove('order');
        }

        function updateItem(index, field, value) {
            _cartItems[index][field] = value;
            localStorageService.set('order', _cartItems);
        }

        function checkPromoCode (request) {
            return Restangular.all('/orders/checkPromo').customPOST(request)
        }

        function sendRequest(data) {
            var request = Object.assign({}, data);
            request.items = getItemSync();
            Restangular.all('/orders').customPOST(request)
                .then(function(res) {
                    var total = 0;
                    request.items.forEach(function(item) {
                        if (item.type == 'wine') total += 1;
                    });
                    _clear();
                    $state.go('thanks', {
                        full_name:request.full_name,
                        address:request.address,
                        quantity: total,
                    });
                })
        }

        service.addItem = addItem;
        service.getItemSync = getItemSync;
        service.remove = remove;
        service.updateItem = updateItem;
        service.checkPromoCode = checkPromoCode;
        service.sendRequest = sendRequest;

        return service;
    }
})();
(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('HttpCacheService', HttpCacheService);

    HttpCacheService.$inject = ['Restangular', '$cacheFactory'];

    function HttpCacheService(Restangular, $cacheFactory) {
        var service = {};
        var cache = $cacheFactory('http');

        function init () {
            Restangular.setDefaultHttpFields({cache: cache});
            Restangular.setResponseInterceptor(function (response, operation) {
                if (operation === 'put' || operation === 'post' || operation === 'remove') {
                    service.clear();
                }

                return response;
            })
        }

        function clear() {
            cache.removeAll();
        }

        service.clear = clear;
        service.init = init;

        return service;
    }
})();
(function () {
    'use strict';
    angular
        .module('app.services')
        .service('RestInterceptor', RestInterceptor);

    RestInterceptor.$inject = ['$rootScope', '$http', '$state', '$timeout', 'Restangular', 'localStorageService'];

    function RestInterceptor($rootScope, $http, $state, $timeout, Restangular, localStorageService) {

        var checkInterval = 10000;
        var hasValidationError = false;

        function init() {
            _checkQueueLogs();

            _setRequestInterceptor();
            _setResponseInterceptor();
            _setErrorInterceptor();
        }

        /**
         * Check queue logs for sent to server
         * @private
         */
        function _checkQueueLogs() {
            var tickId = $timeout(function tick() {
                var errors = localStorageService.get('errors') || [];
                if (errors.length) {
                    _logErrorOnServer(errors, function () {
                        tickId = $timeout(tick, checkInterval)
                    });
                }
            }, checkInterval);
        }

        /**
         * redirect 404 page or sent error to backend
         * @private
         */
        function _setErrorInterceptor() {
            Restangular.setErrorInterceptor(function(response) {
                clearValidationError();
                $rootScope.processing = false;
                if (response.status === 400) { //validation error
                    hasValidationError = true;
                    setValidationError(response.data.errors);
                }
                else if (response.status === 404) {
                    $state.go('404');
                } else {
                    var errorInfo = 'no any info :(';
                    if (navigator) {
                        errorInfo = {
                            actionUrl: response.config.url,
                            browserName: navigator.vendor,
                            onLine: navigator.onLine,
                            cookieEnabled: navigator.cookieEnabled,
                            doNotTrack: navigator.doNotTrack,
                            platform: navigator.platform,
                            date: new Date()
                        }
                    }
                    _alertError(response.data ?  response.data.errors : null);
                    _logErrorOnServer(errorInfo);
                }

                return false;
            });
        }

        /**
         * @private
         */
        function _setRequestInterceptor() {
            Restangular.addFullRequestInterceptor(function (element, operation, route, url, headers, params, httpConfig) {
                headers['X-Requested-With'] = 'XMLHttpRequest';
                $rootScope.processing = true;
            })
        }

        /**
         * @private
         */
        function _setResponseInterceptor() {
            Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
                $rootScope.processing = false;
                if (operation == 'post') {
                    clearValidationError();
                }
                return data;
            });
        }

        /**
         * Show errors with sweetalert 2
         * @param errors
         * @private
         */
        function _alertError(errors) {
            var messages = [];
            if (errors) {
                if (errors.length) {
                    errors.forEach(function(item) {
                        messages.push(item.message);
                    });
                }
            }
            swal({
                title: "Error",
                type: "error",
                text: messages.join(' ') || 'Something going wrong!',
                closeOnConfirm: true
            });
        }

        /**
         *
         * @param errorInfo: array | object
         * @param next: fn
         * @private
         */
        function _logErrorOnServer(errorInfo, next) {

            $http({
                method: 'POST',
                url: '/log',
                data: errorInfo
            }).then(function (res) {
                //@TODO: add page for success log

                var errors = localStorageService.get('errors') || [];
                if (errors.length) {
                    localStorageService.remove('errors');
                }

            }, function (err) {
                var errors = localStorageService.get('errors');
                if (!errors) {
                    errors = [];
                }
                if (Array.isArray(errorInfo)) {
                    errors = errorInfo;
                } else {
                    errors.push(errorInfo);
                }

                localStorageService.set('errors', errors);

            }).finally(next || angular.noop);
        }

        /**
         * Set validation error under inputs
         * @param err = Array of errors
         */
        function setValidationError(err) {
            if (!err || !err.length ) {
                return false;
            }

            var $el, $parent;

            err.forEach(function (item) {
                $el = $('[name='+item.params.field+']');
                $parent = $el.parent();
                if (item.params) {
                    $parent
                        .addClass('has-danger');

                    $('<div class="form-control-feedback text-left js-alert">'+item.message+'</div>').insertAfter($el);
                    $el
                        .addClass('form-control-danger');
                }
            })

        }

        function clearValidationError() {
            if (hasValidationError) {
                var $el = $('input, textarea');
                $el.removeClass('form-control-danger');
                $el.parent().removeClass('has-danger');
                $('.js-alert').remove();

                hasValidationError = false;
            }
        }

        this.init = init;
        this.setValidationError = setValidationError;
        this.clearValidationError = clearValidationError;
    }
})();
/**
 * Created by Riskey on 5/9/17.
 */
(function () {
    'use strict';

    angular.module('app.ui')
        .filter('hasFieldError', [function () {
            return function (form, field) {
                try {
                    if (form && typeof form[field] === 'undefined') {
                        return false;
                    }
                    if (!form[field].$dirty && !form.$submitted) {
                        return false;
                    }
                    return form[field].$invalid;
                } catch ($e) {}

            };
    }]);

    angular.module('app.ui')
        .filter('prettyNumber', [function () {
            return function (num) {
                return String(num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
            };
        }]);

    angular.module('app.ui')
        .filter('someSelected', [function () {
            return function (array) {
                if (!array) {
                    return false;
                }
                if (!Array.isArray(array)) {
                    return false;
                }
                return array.some(function (item) {
                    return item.selected;
                });
            };
        }]);

    angular.module('app.ui')
        .filter('toFixed', [function () {
            return function (number, count) {
                number = number || 0;
                count = count || 0;
                number = parseFloat(number);
                return number.toFixed(count);
            };
        }]);

    angular.module('app.ui')
        .filter('parseStringDate', function () {
            return function (date, seperator) {
                var exploded_date = date.split(' ');
                return (seperator === 'date') ? exploded_date[0] : exploded_date[1];
            };
        });

    angular.module('app.ui')
        .filter('hideSelected', function () {
            return function (items, hideSelected) {
                var filteredItems = [];
                if (!hideSelected) { return items; }
                else {
                    items.map (function (item) {
                        if (!item.selected) { filteredItems.push(item); }
                    });
                    return filteredItems;
                }
            };
        });

    angular.module('app.ui')
        .filter('filteredBy', function () {
            return function (items, prop, value, reverse) {
                var out = [];

                items.map (function (item) {
                    var filtered = item.data.filter(function (d) {
                        if (d.hasOwnProperty(prop)) {
                            return reverse ? d[prop] !== value : d[prop] == value;
                        }
                    });
                    if (filtered.length) {
                        out = out.concat(item);
                    }
                });

                return out;
            }
        });
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('CommonService', CommonService);

    CommonService.$inject = ['Restangular'];

    function CommonService(Restangular) {
        var service = {};

        function getAppSettings() {
            return Restangular.all('settings').customGET();
        }

        service.getAppSettings = getAppSettings;

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('ContactService', ContactService);

    ContactService.$inject = ['Restangular'];

    function ContactService(Restangular) {
        var service = {};

        var _contactRoute = Restangular.all('/pages/contact');

        function get() {
            return _contactRoute.customGET();
        }

        function subscribeToNews(data) {
            return _contactRoute.customPOST(data, 'subscribe');
        }

        service.get = get;
        service.subscribeToNews = subscribeToNews;

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('EventsService', EventsService);

    EventsService.$inject = ['Restangular'];

    function EventsService(Restangular) {
        var service = {};
        var _events = [];

        var _eventsRoute = Restangular.all('events');

        function get() {
            return _eventsRoute.customGET();
        }

        function getById(id) {
            return _eventsRoute.one(id).get();
        }

        service.get = get;
        service.getById = getById;
        service.events = function () { return _events; };

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('NewsService', NewsService);

    NewsService.$inject = ['Restangular'];

    function NewsService(Restangular) {
        var service = {};
        var _news = [];

        var _newsRoute = Restangular.all('news');

        function get() {
            return _newsRoute.customGET();
        }

        function getById(id) {
            return _newsRoute.one(id).get();
        }

        service.get = get;
        service.getById = getById;
        service.news = function () { return _news; };

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('PagesService', PagesService);

    PagesService.$inject = ['Restangular'];

    function PagesService(Restangular) {

        var service = {};

        var _homeRoute = Restangular.all('/pages/home');
        var _aboutRoute = Restangular.all('/pages/about');
        var _membershipRoute = Restangular.all('/pages/club-membership');

        function getAbout() {
            return _aboutRoute.customGET();
        }

        function getHome() {
            return _homeRoute.customGET();
        }

        function getMembership() {
            return _membershipRoute.customGET();
        }

        function addMembership(data) {
            return Restangular.all('/club-membership').customPOST(data);
        }


        service.getAbout = getAbout;
        service.getHome = getHome;
        service.getMembership = getMembership;
        service.addMembership = addMembership;

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('ToursService', ToursService);

    ToursService.$inject = ['Restangular'];

    function ToursService(Restangular) {
        var service = {};
        var _tours = [];

        var _toursRoute = Restangular.all('pages/wine-tour');

        function get() {
            return _toursRoute.customGET();
        }

        service.get = get;
        service.tours = function () { return _tours; };

        return service;
    }
})();
/**
 * Created by Riskey on 4/9/17.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('WinesService', WinesService);

    WinesService.$inject = ['Restangular'];

    function WinesService(Restangular) {
        var service = {};
        var _wines = [];

        var _winesRoute = Restangular.all('/pages/our-wines');

        function get() {
            return _winesRoute.customGET();
        }

        function getById(id) {
            return _winesRoute.one(id).get();
        }

        function postFeedback(id, data) {
            return Restangular.all('/products').one(id).customPOST(data)
        }

        service.get = get;
        service.getById = getById;
        service.postFeedback = postFeedback;
        service.wines = function () { return _wines; };

        return service;
    }
})();
(function () {
    'use strict';
    angular
        .module('app.directives')
        .directive('addToCart', addToCart);

    addToCart.$inject = ['CartService'];

    function addToCart(CartService) {


        function linkFn (scope, el) {

            scope.add = function() {
                var item = scope.presave();
                if (item) {
                    CartService.addItem(item);
                    scope.aftersave();
                }
            }

        }

        return {
            restrict: "E",
            scope: {
                presave: '&',
                aftersave: '&'
            },
            link: linkFn,
            template: '<button type="button" class="btn-black" ng-click="add()">ADD TO CART</button>'
        };
    }

})();
(function () {
    'use strict';
    angular
        .module('app.directives')
        .directive('cart', cartDirective);

    function cartDirective() {

        CartCtrl.$inject = ['$rootScope', 'CartService'];

        function CartCtrl ($rootScope, CartService) {
            var vm = this;

            function ngOnInit() {
                vm.cartItems = CartService.getItemSync();
            }

            ngOnInit();

            vm.remove = function (index) {
                CartService.remove(index);
            };

            vm.updateQuantity = function(quantity, index) {
            if (!quantity) {
                quantity = 1;
            }
            CartService.updateItem(index, 'quantity', quantity);
        };
        
        }

        return {
            restrict: "E",
            scope: {},
            templateUrl: 'app/views/partials/minicart.html',
            controller: CartCtrl,
            controllerAs: 'vm'
        };
    }

})();