const Users = require('db').model('Users');
const Pages = require('db').model('Pages');
const Settings = require('db').model('Settings');
const async = require('async');
module.exports = () => {
    let pages = ['home', 'our-wines', 'wine-tours', 'club-membership', 'contact'];
    let settings = ['club-member-discount', 'private-tour-price'];

    async.map(pages, page => {
        Pages.find({ type: page}, (err, foundPage) => {
            if (err) {
                throw Error(`Can't install collection ${page}, reason: `, err);
            }
            //insert new 1
            if (!foundPage || !foundPage.length) {
                Pages.create({type: page})
            }
        })
    });

    async.map(settings, item => {
        Settings.findOne({ type: item}, (err, discount) => {
            if (err) {
                throw Error(`Can't install collection, reason: `, err);
            }
            //insert new 1
            if (!discount) {
                Settings.create({ type: item, amount: 0 });
            }
        });
    });



};