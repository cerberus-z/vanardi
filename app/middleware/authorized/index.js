const config = require('config')
    , jwt = require('jsonwebtoken')
    , User = require('db').model('Users');

function clearCookie(req, res) {
    if (req.cookies && req.cookies.token) {
        res.clearCookie('token', {path: '/'});
    }
}

module.exports.isAuthenticated = (req, res, next) => {
    let token = req.cookies.token || req.headers['x-access-token'];
    if (token) {
        //verify secret and check exp
        jwt.verify(token, config.get('jwt:superSecret'), (err, decoded) => {
            if (err) {
                clearCookie(req, res);
                res.redirect('/auth');
            }
            //issues with decoding
            if (!decoded) {
                return clearCookie(req, res);
                // res.redirect('/');
            }
            //if everything is good, save to request
            req.email = decoded.email;
            next();
        });

    } else {
        //if there is no token
        res.redirect('/auth');
    }
};

module.exports.isUnAuthenticated = (req, res, next) => {
    if (!req.cookies.token && !req.headers['x-access-token']) {
        clearCookie(req, res);
        next();
    } else {
        res.redirect('/admin');
    }
};

module.exports.isAdmin = (req, res, next) => {
	User.findOne({
        email: req.email
    })
        .select('email password role')
        .exec((err, user) => {
            if (err) {
                res.redirect('back');
            }
            return user.role === 'admin'
                ? next()
                : res.redirect('back');
        });
};
