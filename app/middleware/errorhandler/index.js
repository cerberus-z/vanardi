const config = require('config');
const app = require('express')();

/**
 *  catch 404 and forward to error log errors
 */
module.exports.error404Catcher = function (req, res, next)
{
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
};

/**
 * log error and forward to @function:clientErrorHandler
 */
module.exports.logErrors = function (err, req, res, next)
{
    // console.log(`${err.stack}`);
    next(err);
};

/**
 * client side error handling for backend
 */
module.exports.clientErrorHandler = function (err, req, res, next)
{
    const workflow = require('helpers/workflow')(req, res);
    if (req.xhr) {
        workflow.pushError('Something going wrong, please try later!');
        return workflow.emit('exception');
    } else {
        next(err);
    }
};

/**
 * final error handler
 */
module.exports.errorHandler = function (err, req, res, next)
{
    let isDevelopment = app.get('env') === 'development';
    if (err.status === 404) {
        res.render('admin/error/404');
    } else {
        res.status(err.status || 500);
        res.render('admin/error/500', {
            message:isDevelopment ? err.message : "",
            error: isDevelopment ? err : {},
        });
    }
};

