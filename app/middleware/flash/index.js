function flashMessage(req, res, next) {
    // if there's a flash message in the session request
    res.locals.messages = req.flash('messages');
    next();
}

module.exports = flashMessage;