const Joi = require('joi');
const joiValidate = require('helpers/joi');

module.exports.outputValidator = (req) => {
    let readSchema = {
        id: Joi.string(),
        name: Joi.string().required(),
        role: Joi.string(),
        email: Joi.string().email().required(),
        phone: Joi.string(),
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.updateValidator = (req) => {
    let readSchema = {
        name: Joi.string().required(),
        role: Joi.string(),
        password: Joi.description('password'),
        email: Joi.string().email().required(),
        phone: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.inputValidator = (req) => {
    let readSchema = {
        name: Joi.string().required(),
        role: Joi.string(),
        password: Joi.string().min(6).max(12).required(),
        email: Joi.string().email().required(),
        phone: Joi.string(),
    };

    return joiValidate(Joi.object().keys(readSchema));
};