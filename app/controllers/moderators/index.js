const  User = require('db').model('Users')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id',
            filters: { role: 'moderator'}
        };

        User.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({virtuals: true}), callback);
                },
                (err, moderators) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.moderators = moderators;
                    workflow.emit('render', 'admin/moderators');
                }
            )
        });
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            requestObject.role = 'moderator';
            let user = new User(requestObject);
            user.save((err, newUser) =>  {
                if (err || !newUser) {
                    return workflow.validationError(err);
                }
                workflow.emit('redirect');
            });
        });

        workflow.emit('validate');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('redirect');
                }
                requestObject = validRequestObject;
                workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            User.update({ _id: req.params.id }, requestObject, (err, moderator) => {
                if (err || !moderator) {
                    return workflow.validationError(err);
                }
                return workflow.emit('flashMessage', 'Successfully updated!');
            });
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            User.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};