const Page = require('db').model('Pages')
    , Stores = require('db').model('Stores')
    , async = require('async');

module.exports = {
    get: function (req, res) {
        let workflow = require('helpers/workflow')(req, res);

        async.parallel({
            data: (callback) => {
                Page.findOne({ type: 'contact' }, (err, page) => {
                    if (err || !page) {
                        return workflow.emit('exception', err);
                    }

                    callback(null, page.content);
                });
            },
            stores: (callback) => {
                Stores
                    .find({})
                    .exec((err, stores) => {
                        if (err) {
                            return workflow.emit('exception', err);
                        }
                        callback(null, stores);
                    });
            }
        }, (err, results) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            workflow.outcome.data = results.data;
            workflow.outcome.stores = results.stores;

            console.log(workflow.outcome);

            if (req.xhr) {
                workflow.emit('response');
            } else {
                workflow.emit('render', 'admin/contact');
            }
        });
    },
    
    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        Page.findOne({ type: 'contact' }, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }

            //Store modified data
            page.content.facebook = req.body.facebook;
            page.content.phone = req.body.phone;
            page.content.email = req.body.email;
            page.content.address = req.body.address;
            page.content.twitter = req.body.twitter;
            page.content.youtube = req.body.youtube;
            page.content.google = req.body.google;
            page.content.instagram = req.body.instagram;

            Page.update({type: 'contact'}, page, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.emit('flashMessage', 'Successfully updated!');
            });

        });
    }
};