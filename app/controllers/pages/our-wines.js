const express = require('express')
    , OurWines = require('db').model('Pages');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        OurWines.findOne({type: 'our-wines'}, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }
            workflow.outcome.data = page.content;
            workflow.emit('render', 'admin/ourwines');
        });
    },
    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        OurWines.findOne({type: 'our-wines'}, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }

            let fileUploader = require('helpers/api/fileUploader')(req, page.content.images);
            if (fileUploader.hasErrors()) {
                return workflow.emit('exception', fileUploader.errors.message);
            }

            //Store modified data
            page.content.title = req.body.title;
            page.content.secondary_title = req.body.secondary_title;
            page.content.images = fileUploader.data;

            OurWines.update({type: 'our-wines'}, page, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.emit('flashMessage', 'Successfully updated!');
            });

        });
    }
};