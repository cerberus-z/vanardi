const express = require('express')
    , AboutItem = require('db').model('AboutItem')
    , del = require('del')
    , config = require('nconf')
    , Response = require('helpers/workflow/Response.js')
    , validator = require('./validators')
    , async = require('async');

function _removeImage(item) {
    let imagePath = `${config.get('uploadPath')}/${item.image}`;
    del(imagePath).then(paths => {
        //console.log('Deleted files and folders:\n', paths.join('\n'));
    });
}

module.exports = {
    get: function (req, res) {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: 'date'
         };

        AboutItem.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.ioAboutValidator(req)(item.toObject({virtuals: true}), callback);
                },
                (err, items) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.items = items;
                    if (req.xhr) {
                        workflow.emit('response');
                    } else {
                        workflow.emit('render', 'admin/about');
                    }
                }
            )
        });
    },

    create: function (req, res) {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            // Everything went fine
            validator.ioAboutValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                requestObject = validRequestObject;
                if (req.files.length) {
                    req.files.forEach( (file) => requestObject.image = file.filename);
                }

                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            let aboutItem = new AboutItem(requestObject);
            aboutItem.save((err, newAboutItem) =>  {
                if (err || !newAboutItem) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('flashMessage', 'Successfully created!');
            });
        });

        workflow.emit('validate');
    },

    update: function (req, res) {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.ioAboutValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }
                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            AboutItem.findOne({ _id: req.params.id }, (err, item) => {
                if (err || !item) {
                    return workflow.emit('exception', err);
                }

                if (req.files.length) {
                    req.files.forEach( (file) => requestObject.image = file.filename);
                    _removeImage(item);
                }

                AboutItem.update({ _id: req.params.id }, requestObject, (err, item) => {
                    if (err || !item) {
                        return workflow.emit('exception', err);
                    }

                    return workflow.emit('flashMessage', 'Successfully updated!');
                });
            });
        });

        workflow.emit('validate');
    },

    remove: function (req, res) {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('remove', () => {
            AboutItem.findById(req.params.id , (err, item) => {
                if (err || !item) {
                    return workflow.emit('exception', err);
                }
                _removeImage(item);
                item.remove();

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};