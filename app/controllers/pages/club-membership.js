const express = require('express')
    , async = require('async')
    , ClubMembership = require('db').model('Pages')
    , Settings = require('db').model('Settings')
    , sanitizeHtml = require('sanitize-html');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        async.parallel({
            content: (callback) => {
                ClubMembership.findOne({type: 'club-membership'}, (err, page) => {
                    if (err || !page) {
                        return workflow.emit('exception', err);
                    }

                    callback(null, page.content);
                });
            },
            discount: (callback) => {
                Settings
                    .findOne({ type: "club-member-discount"})
                    .exec((err, discount) => {
                        if (err) {
                            return workflow.emit('exception', err);
                        }
                        callback(null, discount.amount);
                    });
            }
        }, (err, results) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            workflow.outcome.data = results.content;
            workflow.outcome.discount = results.discount;

            if (req.xhr) {
                workflow.emit('response');
            } else {
                workflow.emit('render', 'admin/club-membership');
            }
        });
    },
    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        ClubMembership.findOne({type: 'club-membership'}, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }

            let fileUploader = require('helpers/api/fileUploader')(req, page.content.images);
            if (fileUploader.hasErrors()) {
                return workflow.emit('exception', fileUploader.errors.message);
            }

            //Store modified data
            page.content.title = req.body.title;
            page.content.secondary_title = req.body.secondary_title;
            page.content.description = sanitizeHtml(req.body.description, {
                allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
            });
            page.content.images = fileUploader.data;

            Settings.update({ type: 'club-member-discount' }, { amount: req.body.discount}, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
            });

            ClubMembership.update({ type: 'club-membership' }, page, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.emit('flashMessage', 'Successfully updated!');
            });

        });
    }
};