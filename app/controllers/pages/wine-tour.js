const express = require('express')
    , sanitizeHtml = require('sanitize-html')
    , WineTours = require('db').model('Pages');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        WineTours.findOne({type: 'wine-tours'}, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }
            workflow.outcome.data = page.content;
            workflow.emit('render', 'admin/wine-tours');
        });
    },
    
    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        WineTours.findOne({type: 'wine-tours'}, (err, page) => {
            if (err || !page) {
                return workflow.emit('exception', err);
            }

            let fileUploader = require('helpers/api/fileUploader')(req, page.content.images);
            if (fileUploader.hasErrors()) {
                return workflow.emit('exception', fileUploader.errors.message);
            }
            console.log(req.body.description);
            //Store modified data
            page.content.title = req.body.title;
            page.content.secondary_title = req.body.secondary_title;
            page.content.description = sanitizeHtml(req.body.description, {
                allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
            });
            page.content.images = fileUploader.data;
            console.log(page.content.description);

            WineTours.update({type: 'wine-tours'}, page, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.emit('flashMessage', 'Successfully updated!');
            });

        });
    }
};