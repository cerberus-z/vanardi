const Joi = require('joi');
const joiValidate = require('helpers/joi');

module.exports.ioAboutValidator = (req) => {

    let readSchema = {
        id: Joi.string().description('Id'),
        title: Joi.string(),
        description: Joi.string().description('Order Client name'),
        date: Joi.description('date'),
        image: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};
