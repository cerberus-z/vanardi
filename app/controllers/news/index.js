const News =  require('db').model('News')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        let pagedSettings = {
            sort: req.query.sort || '_id'
        };

        News.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, news) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.news = news;
                    if (req.xhr) {
                        workflow.emit('response');
                    } else {
                        workflow.emit('render', 'admin/news');
                    }
                }
            )
        });
    },

    show: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('edit', () => {
            News
                .findOne({ _id: req.params.id })
                .exec((err, news) => {
                    if (err || !news) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.news = news;
                    workflow.emit('response');
                });
        });

        workflow.emit('edit');
    },

    create: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.emit('render', 'admin/news/update');
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            if (req.files.length) {
                req.files.forEach( (file) => requestObject.avatar = file.filename);
            }

            let news = new News(requestObject);
            news.save((err, newNews) =>  {
                if (err || !newNews) {
                    return workflow.validationError(err);
                }
                workflow.emit('redirect', '/admin/news/' + newNews._id);
            });
        });

        workflow.emit('validate');

    },

    edit: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('edit', () => {
            News
                .findOne({ _id: req.params.id })
                .exec((err, news) => {
                    if (err || !news) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.news = news;
                    workflow.emit('render', 'admin/news/update');
                });
        });

        workflow.emit('edit');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            News.findOne({ _id: req.params.id }, (err, news) => {
                if (err || !news) {
                    return workflow.emit('exception', err);
                }

                if (req.files.length) {
                    req.files.forEach( (file) => requestObject.avatar = file.filename);
                    // _removeImage(event);
                }

                News.update({ _id: req.params.id }, requestObject, (err, news) => {
                    if (err || !news) {
                        return workflow.emit('exception', err);
                    }

                    return workflow.emit('flashMessage', 'Successfully updated!');
                });
            });
            // Event.update({ _id: req.params.id }, requestObject, (err, event) => {
            //     if (err || !event) {
            //         return workflow.emit('exception', err);
            //     }
            // });

            // workflow.emit('flashMessage', 'Successfully created!');
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            News.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};