const Joi = require('joi');
const joiValidate = require('helpers/joi');
const DATE_REGEXP = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

module.exports.outputValidator = (req) => {
    let readSchema = {
        id: Joi.string(),
        title: Joi.string().required(),
        description: Joi.string().required(),
        avatar: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP)
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.inputValidator = (req) => {
    let readSchema = {
        title: Joi.string().required(),
        description: Joi.string().required(),
        avatar: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP)
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.updateValidator = (req) => {
    let readSchema = {
        id: Joi.string(),
        title: Joi.string().required(),
        description: Joi.string().required(),
        avatar: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP)
    };

    return joiValidate(Joi.object().keys(readSchema));
};