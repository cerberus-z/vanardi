const Stores =  require('db').model('Stores')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id'
        };

        Stores.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (store, callback) => {
                    validator.ioAboutValidator(req)(store.toObject({ virtuals: true }), callback);
                },
                (err, stores) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.stores = stores;
                    workflow.emit('response');
                }
            )
        });
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            // Everything went fine
            validator.ioValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            Stores.create(requestObject, (err, store) => {
                if ( err ) {
                    return workflow.validationError(err);
                }

                return workflow.emit('flashMessage', 'Successfully created!');
            });
        });

        workflow.emit('validate')
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.ioValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }
                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Stores.findOne({ _id: req.params.id }, (err, item) => {
                if (err || !item) {
                    return workflow.emit('exception', err);
                }

                Stores.update({ _id: req.params.id }, requestObject, (err, item) => {
                    if (err || !item) {
                        return workflow.emit('exception', err);
                    }

                    return workflow.emit('flashMessage', 'Successfully updated!');
                });
            });
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('remove', () => {
            Stores.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};