const Joi = require('joi');
const joiValidate = require('helpers/joi');

module.exports.ioValidator = (req) => {
    let readSchema = {
        name: Joi.string(),
        address: Joi.string().required(),
        phone: Joi.string().required()
    };

    return joiValidate(Joi.object().keys(readSchema));
};