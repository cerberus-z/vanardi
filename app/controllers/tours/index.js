const Tour = require('db').model('Tour')
    , Settings = require('db').model('Settings')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        let pagedSettings = {
            sort: req.query.sort || '_id'
        };

        async.parallel({
            tours: (callback) => {
                Tour.pagedFind(pagedSettings, (err, result) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }

                    async.mapLimit(result.data, 100,
                        (item, callback) => {
                            validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                        },
                        (err, tours) => {
                            if (err) {
                                return workflow.validationError(err);
                            }

                            callback(null, tours);
                        }
                    )
                });
            },
            privatePrice: (callback) => {
                Settings.findOne({ type: "private-tour-price" }, (err, price) => {
                    workflow.outcome.privateTourPrice = price.amount;

                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    callback(null,  price.amount);
                });
            }
        }, (err, results) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            workflow.outcome.tours = results.tours;
            workflow.outcome.privatePrice = results.privateTourPrice;

            if (req.xhr) {
                workflow.emit('response');
            } else {
                workflow.emit('render', 'admin/tours');
            }
        });
    },

    show: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('edit', () => {
            Tour
                .findOne({ _id: req.params.id })
                .exec((err, tour) => {
                    if (err || !tour) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.tour = tour.toObject({virtuals: true});
                    workflow.emit('response');
                });
        });

        workflow.emit('edit');
    },

    store: (req,res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        console.log(req.body);
        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            let dates = requestObject.date.split(',');
            async.eachLimit(dates, 10, (date, done) => {
                requestObject.date = date;
                let tour = new Tour(requestObject);
                tour.save(done);
            }, (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('flashMessage', 'Successfully created!');
            })
        });

        workflow.emit('validate');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Tour.update({ _id: req.params.id }, requestObject, (err, tour) => {
                if (err || !tour) {
                    return workflow.emit('exception', err);
                }
            });

            return workflow.emit('flashMessage', 'Successfully updated!');
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Tour.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    },

    privateTourPrice: (req, res) => {
        Settings.update({ type: "private-tour-price" }, { amount: req.body.privateTourPrice}, (err) => {
            if (err) {
                return workflow.emit('exception', err);
            }
        });

        let workflow = require('helpers/workflow')(req, res);
        return workflow.emit('flashMessage', 'Successfully updated!');
    },
};