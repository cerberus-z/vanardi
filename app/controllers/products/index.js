const Products =  require('db').model('Products')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , Pairings = require('db').model('Pairings')
    , config = require('nconf')
    , del = require('del')
    , moment = require('moment')
    , async = require('async');

function getProductById (id, callback) {
    Products.findOne({_id: id}, (err, data) => {
        if (err || !data) {
            callback(err)
        }
        callback && callback(null, data);
    });
}

function _removeImage(item) {
    let imagePath = `${config.get('uploadPath')}/${item.image ? item.image : item.avatar}`;
    del(imagePath).then(paths => {});
}

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id'
        };
        Products.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, products) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.products = products;
                    workflow.emit('render', 'admin/products/index');
                }
            )
        });
    },

    create: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        Pairings
            .find({})
            .exec((err, pairings) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                workflow.outcome.pairings = pairings;

                workflow.emit('render', 'admin/products/update');
            });
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                requestObject = validRequestObject;
                if (req.files.length) {
                    req.files.forEach( (file) => {
                        if (file.fieldname == 'avatar') {
                            requestObject.avatar = file.filename;
                        }
                    });
                }
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            let product = new Products(requestObject);
            product.save((err, newProduct) =>  {
                if (err || !newProduct) {
                    return workflow.emit('exception', err);
                }
                workflow.emit('redirect', `/admin/products/${newProduct._id}`);
            });
        });

        workflow.emit('validate');

    },

    edit: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('edit', () => {
            async.parallel({
                product: (callback) => {
                    Products.
                    findOne({ _id: req.params.id })
                    // .populate({ path: 'pairings' })
                        .exec((err, product) => {
                            if (err || !product) {
                                return workflow.emit('exception', err);
                            }

                            callback(null, product);
                        });
                },
                pairings: (callback) => {
                    Pairings
                        .find({})
                        .exec((err, pairings) => {
                            if (err) {
                                return workflow.emit('exception', err);
                            }

                            callback(null, pairings);
                        });
                }
            }, (err, results) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                workflow.outcome.pairings = results.pairings;
                workflow.outcome.product = results.product;

                workflow.emit('render', 'admin/products/update');
            });
        });

        workflow.emit('edit');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;

                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Products.findOne({_id: req.params.id}, (err, product) => {
                if (err || !product) {
                    return workflow.emit('exception', err);
                }
                if (req.files.length) {
                    req.files.forEach( (file) => {
                        if (file.fieldname == 'avatar') {
                            requestObject.avatar = file.filename;
                            _removeImage(product);
                        }
                    });
                }
                Products.update({ _id: req.params.id}, requestObject, (err, product) => {
                    if (err || !product) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('flashMessage', 'Successfully updated!');
                });
            });
        });


        if (req.params.gallery) {
            getProductById(req.params.id, (err, result) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                let fileUploader = require('helpers/api/fileUploader')(req, result.gallery);
                if (fileUploader.hasErrors()) {
                    return workflow.emit('exception', fileUploader.errors.message);
                }
                requestObject = {
                    _id: req.params.id,
                    gallery: fileUploader.data
                };

                workflow.emit('update');
            });

        }
        else {
            workflow.emit('validate');
        }
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Products.findOne({_id: req.params.id}, (err, product) => {
                if (err || !product) {
                    return workflow.emit('exception', err);
                }
                _removeImage(product);
                product.remove((err) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('response');
                });
            });
        });

        workflow.emit('remove');
    },

    storeMedal: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        let product;

        workflow.on('getProduct', () => {
            if (!req.body.prodId) {
                workflow.pushError('Can\'t get product for attach medal');
                return workflow.emit('exception');
            }

            getProductById(req.body.prodId, (err, data) => {
                if (err || !data) {
                    return workflow.emit('exception', err);
                }
                product = data;
                workflow.emit('validate');
            });
        });

        workflow.on('validate', () => {
            validator.medalsValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                requestObject = validRequestObject;

                if (req.files.length) {
                    req.files.forEach( (file) => requestObject.image = file.filename);
                }

                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            product.medals = product.medals || [];
            let ln = product.medals.push(requestObject);

            product.save((err, uproduct) =>  {
                if (err || !uproduct) {
                    return workflow.emit('exception', err);
                }
                workflow.outcome.id = uproduct.medals[ln-1]._id;
                workflow.emit('response');
            });
        });

        workflow.emit('getProduct');
    },

    updateMedal: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        let product;

        workflow.on('getProduct', () => {
            if (!req.body.prodId) {
                workflow.pushError('Can\'t get product for attach medal');
                return workflow.emit('exception');
            }

            getProductById(req.body.prodId, (err, data) => {
                if (err || !data) {
                    return workflow.emit('exception', err);
                }
                product = data;
                workflow.emit('validate');
            });
        });

        workflow.on('validate', () => {
            validator.medalsValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }
                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            let index;
            product.medals.forEach((item, i) => {
                if (item._id == req.params.id) { index = i; }
            });

            if (typeof index == void 0) {
                workflow.pushError('Cant find medal');
                return workflow.emit('exception');
            }

            if (req.files.length) {
                req.files.forEach( file => requestObject.image = file.filename);
                _removeImage(product.medals[index]);
            }

            product.medals[index] = requestObject;
            product.save((err, item) => {
                if (err || !item) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('response');
            });
        });

        workflow.emit('getProduct');
    },

    removeMedal: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('remove', () => {
            getProductById(req.body.prodId, (err, product) => {
                if (err || !product) {
                    return workflow.emit('exception', err);
                }

                let index;
                product.medals.forEach((item, i) => {
                    if (item._id == req.params.id) { index = i; }
                });

                if (typeof index == void 0) {
                    workflow.pushError('Cant find medal');
                    return workflow.emit('exception');
                }

                _removeImage(product.medals[index]);
                product.medals.splice(index, 1);

                product.save((err, item) => {
                    if (err || !item) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('response');
                })
            });
        });

        workflow.emit('remove');
    },

    getProduct: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id',
        };

        Products.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, products) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.products = products;
                    workflow.emit('render', 'admin/products/index');
                }
            )
        });
    },

    addFeedback: (req, res) => {
        let reCAPTCHA = require('recaptcha2');
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {

            let recaptcha = new reCAPTCHA({
                siteKey:config.get('reCaptcha:siteKey'),
                secretKey:config.get('reCaptcha:secret')
            });

            req.body.date = moment().format('DD/MM/YYYY');
            req.body.confirmed = 0;

            validator.inputFeedBackValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;

                recaptcha.validate(req.body.captcha)
                    .then(() => {
                        return workflow.emit('store');
                    })
                    .catch(err => {
                        return workflow.emit('exception', recaptcha.translateErrors(err)[0]);
                    });
            });
        });

        workflow.on('store', () => {
            Products.findOne({_id: req.params.id}, (err, product) => {
                if (err || !product) {
                    return workflow.emit('exception', err);
                }

                product.feedback.push(requestObject);
                product.save((err, item) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    workflow.emit('response');
                });
            });
        });


        workflow.emit('validate');
    },

    editFeedback: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        Products.findOne({_id: req.params.prodId}, (err, product) => {
            if (err || !product) {
                return workflow.emit('exception', err);
            }

            if (req.body.action == 1) { //accept
                let feedback = product.feedback.id(req.body.id);
                if (!feedback) {
                    return workflow.emit('exception', 'Cant found feedback');
                }
                feedback.confirmed = 1;
            } else if (req.body.action == 0) { //reject
                product.feedback.remove(req.body.id);
            }

            product.save((err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('response');
            });
        });
    }
};