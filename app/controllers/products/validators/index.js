const Joi = require('joi')
    , joiValidate = require('helpers/joi')
    , mongoose = require('mongoose')
    , DATE_REGEXP = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

module.exports.updateValidator = (req) => {
    let readSchema = {
        _id: Joi.string().description('Product id'),
        name: Joi.string().description('Product name'),
        description: Joi.string().description('Product description')
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.medalsValidator = req => {
    let readMedalSchema = {
        name: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP),
        image: Joi.string()
    };

    return joiValidate(Joi.object().keys(readMedalSchema));
};

module.exports.inputValidator = module.exports.outputValidator = (req) => {
    let readFeedBack = {
        full_name: Joi.string(),
        description: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP),
        rate: Joi.number(),
        confirmed: Joi.number()
    };

    // let readPairingS

    let readSchema = {
        id: Joi.string().description('Product id'),
        name: Joi.string().description('Product name'),
        description: Joi.string(),
        in_stock: Joi.number(),
        price: Joi.number(),
        discount: Joi.number().empty('').default(0),
        tag: Joi.array().items(Joi.string()),
        alcohol_lvl: Joi.number(),
        avatar: Joi.string(),
        barrel_aged: Joi.description('barrel aged'),
        featured: Joi.description('featured'),
        pairings: Joi.array().items(Joi.string()),
        // medals: Joi.array().items(readMedalSchema),
        feedback: Joi.array().items(readFeedBack)
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.inputFeedBackValidator = (req) => {
    let readSchema = {
        full_name: Joi.string(),
        description: Joi.string(),
        date: Joi.string().regex(DATE_REGEXP),
        rate: Joi.number(),
        confirmed: Joi.number()
    };

    return joiValidate(Joi.object().keys(readSchema));
};