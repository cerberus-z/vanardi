const Pairing = require('db').model('Pairings')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        Pairing.pagedFind({}, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.ioValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, pairings) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.pairings = pairings;
                    workflow.emit('render', 'admin/pairings');
                }
            );
        });
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        workflow.on('validate', () => {
            validator.ioValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            requestObject.role = 'club_member';
            let pairing = new Pairing(requestObject);
            pairing.save((err, newPairing) =>  {
                if (err || !newPairing) {
                    return workflow.validationError(err);
                }
                workflow.emit('flashMessage', 'Successfully Created!');
            });
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Pairing.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};