const Settings =  require('db').model('Settings')
    , Page = require('db').model('Pages')
    , Event = require('db').model('Event')
    , Products = require('db').model('Products')
    , Orders = require('db').model('Order')
    , Clients = require('db').model('Client')
    , Tour = require('db').model('Tour')
    , moment = require('moment')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        async.parallel({
            settings: (callback) => {
                Settings.find({}, (err, settings) => {
                    if (err) {
                        callback(err)
                    }
                    let outcome = [];
                    settings.forEach(setting => {
                        outcome.push({
                            type: setting.type,
                            amount: setting.amount
                        });
                    });

                    callback(null, outcome);
                })
            },
            contact: (callback) => {
                Page.findOne({ type: 'contact' }, (err, page) => {
                    if (err || !page) {
                        callback(err || 'Cant find contact info');
                    }
                    let outcome = page.content;

                    callback(null, outcome);
                });
            }
        }, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            workflow.outcome.settings = result.settings;
            workflow.outcome.contacts = result.contact;

            return workflow.emit('response');
        });
    },

    getBadges: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        async.parallel({
            feedback: (callback) => {
                Products.find({
                    'feedback.confirmed': false
                }, (err, products) => {
                    if (err) {
                        callback(err);
                    }
                    let countPendingFeedback = 0;
                    products.forEach(product => {
                        let pendingFeedback = product.feedback.filter(item => item.confirmed == false);
                        countPendingFeedback += pendingFeedback.length;
                    });
                    callback(null, countPendingFeedback);
                })
            },
            orders: (callback) => {
                Orders.count({status: 'pending'}, (err, ordersCount) => {
                    if (err) {
                        callback(err);
                    }
                    callback(null, ordersCount);
                });
            },
            clients: (callback) => {
                Clients.count({
                    status: 'pending',
                    role: 'club_member'
                }, (err, clientsCount) => {
                    if (err) {
                        callback(err);
                    }
                    callback(null, clientsCount);
                })
            }
        }, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            workflow.outcome.data = {
                feedback: result.feedback,
                orders: result.orders,
                clients: result.clients
            };

            return workflow.emit('response');
        });
    },

    dashboard: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        async.parallel({
            club_member: (callback) => {
                Clients.count({ role: 'club_member', status: 'pending' } , (err, clubMembers) => {
                    if (err) {
                        callback(err)
                    }

                    callback(null, clubMembers);
                });
            },
            orders: (callback) => {
                Orders.count({ status: 'pending' } , (err, orders) => {
                    if (err) {
                        callback(err)
                    }

                    callback(null, orders);
                });
            },
            subscribers: (callback) => {
                Clients.count({ role: 'subscriber' } , (err, subscribers) => {
                    if (err) {
                        callback(err)
                    }

                    callback(null, subscribers);
                });
            },
            events: (callback) => {
                Event
                    .count({ date:  { $gte: moment().format("DD/MM/YYYY") }})
                    .exec((err, events) => {
                        if (err) {
                            return workflow.emit('exception', err);
                        }

                        callback(null, events);
                    });
            },
            tours: (callback) => {
                Tour
                    .count({ date:  { $gte: moment().format("DD/MM/YYYY") }})
                    .exec((err, tours) => {
                        if (err) {
                            return workflow.emit('exception', err);
                        }

                        callback(null, tours);
                    });
            }
        }, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            workflow.outcome.club_member = result.club_member;
            workflow.outcome.orders = result.orders;
            workflow.outcome.subscribers = result.subscribers;
            workflow.outcome.events = result.events;
            workflow.outcome.tours = result.tours;

            workflow.emit('render', 'admin/index');
        });
    }
};