const Event = require('db').model('Event')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , sanitizeHtml = require('sanitize-html')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        let pagedSettings = {
            sort: req.query.sort || '_id'
        };

        Event.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, events) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.events = events;
                    if (req.xhr) {
                        workflow.emit('response');
                    } else {
                        workflow.emit('render', 'admin/events');
                    }
                }
            )
        });
    },

    show: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('edit', () => {
            Event
                .findOne({ _id: req.params.id })
                .exec((err, event) => {
                    if (err || !event) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.event = event.toObject({virtuals: true});
                    workflow.emit('response');
                });
        });

        workflow.emit('edit');
    },

    create: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.emit('render', 'admin/events/update');
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            if (req.files.length) {
                req.files.forEach( (file) => requestObject.avatar = file.filename);
            }

            requestObject.description = sanitizeHtml(requestObject.description, {
                allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
            });

            let event = new Event(requestObject);
            event.save((err, newEvent) =>  {
                if (err || !newEvent) {
                    return workflow.validationError(err);
                }
                workflow.emit('redirect', '/admin/events/' + newEvent._id);
            });
        });

        workflow.emit('validate');
    },

    edit: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('edit', () => {
            Event
                .findOne({ _id: req.params.id })
                .exec((err, event) => {
                    if (err || !event) {
                        return workflow.emit('exception', err);
                    }
                    workflow.outcome.event = event;
                    workflow.emit('render', 'admin/events/update');
                });
        });

        workflow.emit('edit');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Event.findOne({ _id: req.params.id }, (err, event) => {
                if (err || !event) {
                    return workflow.emit('exception', err);
                }

                if (req.files.length > 0) {
                    req.files.forEach( (file) => requestObject.avatar = file.filename);
                    // _removeImage(event);
                }

                requestObject.description = sanitizeHtml(requestObject.description, {
                    allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'img' ])
                });

                Event.update({ _id: req.params.id }, requestObject, (err, event) => {
                    if (err || !event) {
                        return workflow.emit('exception', err);
                    }

                    return workflow.emit('flashMessage', 'Successfully updated!');
                });
            });
        });

        workflow.emit('validate');

    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Event.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};