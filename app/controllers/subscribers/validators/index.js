const Joi = require('joi');
const joiValidate = require('helpers/joi');

module.exports.outputValidator = (req) => {
    let readSchema = {
        id: Joi.string().description('id'),
        first_name: Joi.string().description('Client first name'),
        last_name: Joi.string().description('Client last description'),
        email: Joi.string().email(),
        created_at: Joi.date(),
        role: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.updateValidator = (req) => {
    let readSchema = {
        first_name: Joi.string().required().description('Client name'),
        last_name: Joi.string().required().description('Client description'),
        email: Joi.string().required()
    };

    return joiValidate(Joi.object().keys(readSchema));
};
module.exports.ioValidator = (req) => {
    let readSchema = {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().email().required()
    };

    return joiValidate(Joi.object().keys(readSchema));
};