const Joi = require('joi');
const joiValidate = require('helpers/joi');
const DATE_REGEXP = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

module.exports.outputValidator = (req) => {
    let readSchema = {
        id: Joi.string().description('Client id'),
        first_name: Joi.string().description('Client first name'),
        last_name: Joi.string().description('Client last description'),
        email: Joi.string().email().required(),
        birth_date: Joi.string(),
        phone: Joi.string(),
        address: Joi.string(),
        promo_code: Joi.string(),
        status: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.updateValidator = (req) => {
    let readSchema = {
        id: Joi.string().description('Client id'),
        first_name: Joi.string().description('Client first name'),
        last_name: Joi.string().description('Client last description'),
        email: Joi.string().email().required(),
        birth_date: Joi.string(),
        phone: Joi.string(),
        address: Joi.string(),
        status: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.inputValidator = (req) => {
    let readSchema = {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().email().required(),
        birth_date: Joi.string().regex(DATE_REGEXP).required(),
        phone: Joi.string().required(),
        address: Joi.string().required()
    };

    return joiValidate(Joi.object().keys(readSchema));
};