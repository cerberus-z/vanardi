const Client = require('db').model('Client')
    , shortid = require('shortid')
    , config = require('nconf')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id',
            filters: { role: 'club_member'}
        };

        Client.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, members) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.members = members;
                    workflow.emit('render', 'admin/members');
                }
            )
        });
    },

    create: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let reCAPTCHA = require('recaptcha2');
        let recaptcha = new reCAPTCHA({
            siteKey:config.get('reCaptcha:siteKey'),
            secretKey:config.get('reCaptcha:secret')
        });
        let requestObject;

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                recaptcha.validate(req.body.captcha)
                    .then(() => {
                        requestObject = validRequestObject;
                        return workflow.emit('create');
                    })
                    .catch(err => {
                        return workflow.emit('exception', recaptcha.translateErrors(err)[0]);
                    });
            })
        });

        workflow.on('create', () => {
            requestObject.role = 'club_member';
            requestObject.status = 'pending';
            requestObject.promo_code = shortid.generate();
            // let client = new Client(requestObject);

            Client.findOneAndUpdate({ 'email': requestObject.email }, requestObject, (err, client) => {
                console.log(requestObject);
                if (err) {
                    return workflow.emit('exception');
                }

                if (!client) {
                    Client.create(requestObject, (err, newClient) =>  {
                        if (err || !newClient) {
                            return workflow.validationError(err);
                        }
                        if (req.xhr) {
                            workflow.emit('response');
                        } else {
                            return workflow.emit('flashMessage', 'Successfully created!');
                        }
                    });
                } else {
                    if (req.xhr) {
                        workflow.emit('response');
                    } else {
                        return workflow.emit('flashMessage', 'Successfully created!');
                    }
                }
            });
        });

        workflow.emit('validate');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('response');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Client.update({ _id: req.params.id }, requestObject, (err, member) => {
                if (err || !member) {
                    return workflow.emit('exception', err);
                }
                if (req.xhr) {
                    return workflow.emit('response');
                }
                return workflow.emit('flashMessage', 'Successfully updated!');
            });
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Client.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};

