const Joi = require('joi');
const joiValidate = require('helpers/joi');

module.exports.ioValidator = (req) => {
    let readSchema = {
        id: Joi.string(),
        question: Joi.string().required(),
        answer: Joi.string().required()
    };

    return joiValidate(Joi.object().keys(readSchema));
};