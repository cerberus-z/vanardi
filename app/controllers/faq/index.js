const Faq = require('db').model('Faq')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , async = require('async');

module.exports = {
    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id'
        };

        Faq.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.ioValidator(req)(item.toObject({ virtuals:true }), callback);
                },
                (err, faqs) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.faqs = faqs;
                    workflow.emit('render', 'admin/faq');
                }
            )
        });
    },

    store: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.ioValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('create');
            })
        });

        workflow.on('create', () => {
            let faq = new Faq(requestObject);
            faq.save((err, newFaq) =>  {
                if (err || !newFaq) {
                    return workflow.validationError(err);
                }
                workflow.emit('flashMessage', 'Successfully created!');
            });
        });

        workflow.emit('validate');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('validate', () => {
            validator.ioValidator(req)(req.body, (err, validRequestObject) =>  {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }
                requestObject = validRequestObject;
                return workflow.emit('update');
            });
        });

        workflow.on('update', () => {
            Faq.update({ _id: req.params.id }, requestObject, (err, subscriber) => {
                if (err || !subscriber) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('flashMessage', 'Successfully updated!');
            });
        });

        workflow.emit('validate');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Faq.findByIdAndRemove(req.params.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    }
};