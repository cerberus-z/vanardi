const Joi = require('joi')
    , joiValidate = require('helpers/joi')
    , DATE_REGEXP = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;


module.exports.outputValidator = (req) => {
    let readItemSchema = {
        discount: Joi.number(),
        total_cost: Joi.number(),
        unit_cost: Joi.number(),
        ref_id: Joi.string(),
        created_at: Joi.date(),
        id: Joi.string(),
        type: Joi.string(),
        quantity: Joi.number(),
        price: Joi.number(),
        name: Joi.string().optional(),
        date: Joi.string(DATE_REGEXP).optional(),
        time: Joi.string().optional(),
    };

    let readSchema = {
        id: Joi.string().description('Product id'),
        full_name: Joi.string(),
        number: Joi.number(),
        phone: Joi.string(),
        client_name: Joi.string().description('Order Client name'),
        email: Joi.string(),
        address: Joi.string(),
        total_price: Joi.number(),
        real_price: Joi.number(),
        create_at: Joi.date(),
        delivery_date: Joi.string().regex(DATE_REGEXP),
        delivery_time: Joi.string(),
        promo_code: Joi.string().optional(),
        items: Joi.array().items(readItemSchema),
        status: Joi.string()
    };

    return joiValidate(Joi.object().keys(readSchema));
};


module.exports.inputValidator = (req) => {
    let readItemInputSchema = {
        id: Joi.string(),
        type: Joi.string(),
        quantity: Joi.number(),
        price: Joi.number().optional(),
        name: Joi.string().optional(),
        date: Joi.string(DATE_REGEXP).optional(),
        time: Joi.string().optional()
    };
    let readSchema = {
        full_name: Joi.string().description('Client name'),
        delivery_date: Joi.string().regex(DATE_REGEXP),
        delivery_time: Joi.string(),
        email: Joi.string(),
        phone: Joi.string(),
        address: Joi.string(),
        promo_code: Joi.string().optional(),
        items: Joi.array().items(readItemInputSchema)
    };

    return joiValidate(Joi.object().keys(readSchema));
};

module.exports.updateValidator = (req) => {
    let readSchema = {
        full_name: Joi.string(),
        email: Joi.string(),
        phone: Joi.string(),
        address: Joi.string(),
        status: Joi.string(),
        items: Joi.object().keys({
            id: Joi.description('Item id'),
            name: Joi.description('Item name').optional(),
            type: Joi.description('Item type'),
            quantity: Joi.description('Item quantity'),
            price: Joi.description('Item price'),
            // id: Joi.array().items(Joi.string()),
            // type: Joi.array().items(Joi.string()),
            // quantity: Joi.array().items(Joi.string())
        })
    };

    return joiValidate(Joi.object().keys(readSchema));
};