const Order =  require('db').model('Order')
    , Products =  require('db').model('Products')
    , Event =  require('db').model('Event')
    , Tour =  require('db').model('Tour')
    , Client = require('db').model('Client')
    , Settings = require('db').model('Settings')
    , validator = require('./validators')
    , Response = require('helpers/workflow/Response.js')
    , config = require('nconf')
    , async = require('async');


function validateQuantity (requestObject, needStockUpdate, finalCallback) {
    const MIN_QUANTITY = 6;

    let quantity = 0;
    let winesOnly = requestObject.items.filter(item => item.type === 'wine');
    if (winesOnly.length) {
        async.map(winesOnly, (item, callback) => {
            quantity += item.quantity;
            Products.findOne({ _id: item.id }, (err, wine) => {
                let error;

                if (err || !wine) {
                    error = "Product doesn't exist";
                } else if (wine.in_stock < item.quantity) {
                    error = `${wine.name} is out of stock.`;
                }

                if (needStockUpdate) {
                    wine.in_stock -= item.quantity;
                    wine.save(callback);
                } else {
                    callback(error);
                }

            });

        }, (err) => {
            let error = err || '';
            if (quantity < MIN_QUANTITY) {
                error += ' For wine shipping order minimum 6 bottles.';
            }
            finalCallback(error);
        });
    }
    else {
        finalCallback();
    }

}

function calcOrderItems(requestObject, promoCode,  cb) {
    let realPrice = 0;

    if (!requestObject.items.length) {
        cb('No any items for order', null);
    }

    let _setFields = function(item, dataFromDb, callback) {
        item.unit_cost = Math.round(dataFromDb.price);
        if (dataFromDb.discount) {
            item.unit_cost *= Math.round((100 - dataFromDb.discount) / 100);
        }
        item.total_cost = Math.round(item.unit_cost * item.quantity);
        item.name = dataFromDb.name || item.name;
        if (dataFromDb.avatar) {
            item.avatar = dataFromDb.avatar;
        }
        item.ref_id = dataFromDb._id;

        realPrice += item.total_cost;

        callback(null, realPrice);
    };

    async.map(requestObject.items, (item, callback) => {
        switch(item.type) {
            case "wine":
                Products.findOne({ _id: item.id}, (err, product) => {
                    if (err) {
                        return callback(err)
                    }
                    _setFields(item, product, callback);
                });
                break;
            case "event":
                Event.findOne({ _id: item.id}, (err, event) => {
                    if (err) {
                        return callback(err)
                    }
                    _setFields(item, event, callback);
                });
                break;
            case "tour":
                    Tour.findOne({ _id: item.id}, (err, tour) => {
                        if (err) {
                            return callback(err)
                        }
                        _setFields(item, tour, callback);
                    });
                break;
            case "private_tour":
                item.unit_cost = item.price;
                item.total_cost = Math.round(item.unit_cost * item.quantity);
                realPrice += item.total_cost;
                callback(null, realPrice);
                break;
        }
    }, (err, results) => {
        let price = results[0];
        requestObject.real_price = Math.round(price);
        requestObject.total_price = Math.round(price);

        if (promoCode) {
            Settings.findOne({ type: "club-member-discount"}, (err, discount) => {
                requestObject.total_price = Math.round(price * (100 - discount.amount) / 100); //club membership percentage

                cb(err, requestObject);
            });
        } else {
            cb(err, requestObject);
        }
    });
}

function checkPromoCodeValidation(emailAndCode, callback) {
    let filter = emailAndCode;
    filter.role = 'club_member';
    filter.status = 'confirmed';

    Client.findOne(filter, (err, client) => {
        if ( err || !client ) {
            callback("Promo code or email doesn't exist")
        }
        callback();
    });
}

module.exports = {

    get: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let pagedSettings = {
            sort: req.query.sort || '_id'
        };
        Order.pagedFind(pagedSettings, (err, result) => {
            if (err) {
                return workflow.emit('exception', err);
            }
            async.mapLimit(result.data, 100,
                (item, callback) => {
                    validator.outputValidator(req)(item.toObject({ virtuals: true }), callback);
                },
                (err, orders) => {
                    if (err) {
                        return workflow.validationError(err);
                    }
                    workflow.outcome.orders = orders;
                    workflow.emit('render', 'admin/orders');
                }
            )
        });
    },

    create: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let reCAPTCHA = require('recaptcha2');

        let requestObject;
        let promoCode = req.body.promo_code;
        let recaptcha = new reCAPTCHA({
            siteKey:config.get('reCaptcha:siteKey'),
            secretKey:config.get('reCaptcha:secret')
        });

        workflow.on('validate', () => {
            validator.inputValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    workflow.pushError('Request body empty');
                    return workflow.emit('exception');
                }

                recaptcha.validate(req.body.captcha)
                    .then(() => {
                        validateQuantity(validRequestObject, null, (err) => {
                            if (err) {
                                return workflow.emit('exception', err);
                            }
                            requestObject = validRequestObject;
                            return workflow.emit('promoCodeValidation');
                        });
                    })
                    .catch(err => {
                        return workflow.emit('exception', recaptcha.translateErrors(err)[0]);
                    });


            })
        });

        workflow.on('promoCodeValidation', () => {
            if (promoCode) {
                checkPromoCodeValidation({ email: requestObject.email, promo_code: promoCode }, (err) => {
                    if ( err ) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('create');
                });
            } else {
                return workflow.emit('create');
            }
        });

        workflow.on('create', () => {
            calcOrderItems(requestObject, promoCode, (err, outputOfOrder) => {
                let order = new Order(outputOfOrder);
                order.status = 'pending';
                order.save((err, newOrder) =>  {
                    if (err || !newOrder) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('response');
                });
            });
        });

        workflow.emit('validate');
    },

    update: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;
        let promoCode = req.body.promo_code;

        workflow.on('validate', () => {
            validator.updateValidator(req)(req.body, (err, validRequestObject) => {
                if (err) {
                    return workflow.validationError(err);
                }

                if (Object.keys(validRequestObject).length == 0) {
                    return workflow.emit('exception', 'Request body empty');
                }

                requestObject = validRequestObject;

                return workflow.emit('prepareItems');
            });
        });

        workflow.on('prepareItems', () => {
            let items = [];
            /**
             * Body parser receive object if we don't have multiple data
             */
            let isMultipleData = Array.isArray(requestObject.items.type);
            if (isMultipleData) {
                let ln = requestObject.items.type.length;
                for (let i = 0; i < ln; i++) {
                    items.push({
                        id: requestObject.items.id[i],
                        type: requestObject.items.type[i],
                        name: requestObject.items.name[i],
                        quantity: requestObject.items.quantity[i],
                        price: requestObject.items.price[i]
                    });
                }
                requestObject.items = items;
            } else {
                requestObject.items = [requestObject.items];
            }

            return workflow.emit('promoCodeValidation');
        });

        workflow.on('promoCodeValidation', () => {
            if (promoCode) {
                checkPromoCodeValidation({ email: requestObject.email, promo_code: promoCode }, err => {
                    if ( err ) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('calculation');
                });
            } else {
                return workflow.emit('calculation');
            }
        });

        workflow.on('calculation', () => {

            Order.findOne({_id: req.params.id}, (err, order) => {
                if (err || !order) {
                    return workflow.emit('exception', err);
                }
                let orderOldStatus = order.status;

                calcOrderItems(requestObject, promoCode, (err, outputOfOrder) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    requestObject = Object.assign(requestObject, outputOfOrder);
                    validateQuantity(requestObject,
                            (requestObject.status === 'delivered' && orderOldStatus != requestObject.status),
                            (err) => {
                                if (err) {
                                    return workflow.emit('exception', err);
                                }
                                return workflow.emit('update');
                            }
                    )
                });
            });
        });

        workflow.on('update', () => {
            Order.update({_id: req.params.id}, requestObject, (err, updatedOrder) =>  {
                if (err || !updatedOrder) {
                    return workflow.emit('exception', err);
                }
                return workflow.emit('flashMessage', 'Successfully updated!');
            });
        });

        workflow.emit('validate');
    },

    edit: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let requestObject;

        workflow.on('find', () => {
            async.parallel({
                orders: callback => {
                    Order.find({ _id: req.params.id })
                        .populate({ path: 'items.product' })
                        .exec((err, order) => {
                            if (!order) {
                                return workflow.emit('exception', err);
                            }
                            callback(err, order);
                        });
                },
                products: callback => { Products.find({}).exec(callback) },
                events: callback => { Event.find({}).exec(callback) },
                tours: callback => { Tour.find({}).exec(callback) }
            }, (err, results) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                requestObject = results;
                return workflow.emit('validate');
            })
        });

        workflow.on('validate', () => {
            let parallels = {};
            for (let key in requestObject) {
                parallels[key] = (callback) => {
                    const validator = require(`../${key}/validators`);
                    async.mapLimit(requestObject[key], 100,
                        (item, cb) => {
                            validator.outputValidator(req)(item.toObject({ virtuals: true }), cb);
                        },
                        (err, data) => {callback(err, data); }
                    );
                }
            }

            async.parallel(parallels, (err, results) => {
                if (err) {
                    return workflow.emit('exception', err);
                }
                requestObject = results;

                Settings
                    .findOne({ type: "club-member-discount" })
                    .exec((err, data) => {
                        requestObject.discount = data;

                        console.log(requestObject);
                        return workflow.emit('edit');
                    });
            });
        });


        workflow.on('edit', () => {
            workflow.outcome.order = requestObject.orders[0];
            delete requestObject.orders;
            requestObject.tours.map(item => {
                item.name = `Tour (date: ${item.date})`;
            });
            workflow.outcome.additional = requestObject;
            return workflow.emit('render', 'admin/orders/update');
        });

        workflow.emit('find');
    },

    remove: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);

        workflow.on('remove', () => {
            Order.findByIdAndRemove(req.body.id , (err) => {
                if (err) {
                    return workflow.emit('exception', err);
                }

                return workflow.emit('response');
            });
        });

        workflow.emit('remove');
    },

    removeItem: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        workflow.on('removeItem', () => {
            Order.findOne({_id: req.params.id}, (err, order) => {
                if (err || !order) {
                    return workflow.emit('exception', err);
                }
                order.items.pull({_id: req.params.itemId});
                order.save((err) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    return workflow.emit('response');
                });
            });
        });

        workflow.emit('removeItem');
    },

    checkPromoCode: (req, res) => {
        let workflow = require('helpers/workflow')(req, res);
        let body = req.body;
        if (!body.email && !body.promo_code) {
            return workflow.emit('exception', 'Wrong email or promo code');
        }
        checkPromoCodeValidation(body, (err) => {
            if (err) {
                return workflow.emit('exception', 'Wrong email or promo code');
            }
            return workflow.emit('response');
        });
    }
};