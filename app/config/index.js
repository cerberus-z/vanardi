const nconf = require('nconf');
    nconf.argv().env().file({file: require('path').join(__dirname, 'config.json')});
const development = nconf.get('NODE_ENV') === 'development';
const autoIndex = nconf.get('mongoose:autoIndex');

nconf.isDevelopment = () => development;

nconf.autoIndex = () => autoIndex;

module.exports = nconf;