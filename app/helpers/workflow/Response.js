function Response(type, message, params)
{
    this.type = type;
    this.message = message;
    this.params = params;
}

Response.SUCCESS = 'success';
Response.ERROR = 'danger';

module.exports = Response;