const Response = require('./Response.js');

/**
 * workflow for request and response
 * @param req
 * @param res
 * @returns {EventEmitter|*|r}
 */
module.exports = (req, res) =>
{
    const workflow = new (require('events').EventEmitter)(); //closure

    /*
     * Outcome data
     * @type {{success: boolean, errors: Array, data?: any}}
     */
    workflow.outcome = {
        success: false,
        errors: []
    };

    workflow.hasErrors = () => workflow.outcome.errors.length;

    workflow.pushError = message => {
        if (message instanceof Response) {
            workflow.outcome.errors.push(message);
        } else {
            workflow.outcome.errors.push(new Response(Response.ERROR, message.toString()));
        }
    };

    workflow.validationError = err => {
        let outcomeError = {
            status: 400,
            message: '',
            params: ''
        };
        if (err.hasOwnProperty('errors')) { //mongoose validation
            for (let key in err.errors) {
                let field = err.errors[key];
                outcomeError.params = { field: field.path };
                outcomeError.message = `${field.properties.message} : ${field.properties.value || 'receive none'}`;
            }
        } else { //Joi validation
            outcomeError.message = err.details[0].message;
            outcomeError.params = {
                field: err.details[0].path
            };
        }

        workflow.emit('exception', outcomeError);
    };

    /**
     * throw exception via ajax or render error message
     */
    workflow.on('exception', err =>
    {
        console.log(err);

        res.status(500);
        //clear workflow unnecessary fields (if exist)
        workflow.outcome = {
            success: workflow.outcome.success,
            errors: workflow.outcome.errors
        };

        if (err) {
            workflow.pushError(new Response(Response.ERROR,  err.message || err, err.params));
            err.status && res.status(err.status);
        }
        if (req.xhr) {
            workflow.emit('response');
        } else {
            workflow.emit('flashMessage');
        }
    });

    /**
     * @function hook
     * @private
     * check response status and set OK if doesn't find any errors
     * @param fn - custom callback for any task before
     */
    workflow.on('preResponse', fn => {
        !!fn && fn();
        workflow.outcome.success = !workflow.hasErrors();
        if (workflow.outcome.success) {
            res.status(200);
            delete workflow.outcome.errors;
        }
    });

    /**
     * Flash message into view
     */
    workflow.on('flashMessage', response =>
    {
        let messages = [];
        if (!!response) {
            if ('string' == typeof response) {
                response = new Response(Response.SUCCESS, response.toString());
            }
            messages.push(response);
        } else if (workflow.hasErrors()) {
            messages = workflow.outcome.errors;
            // workflow.outcome.errors.forEach(err => messages.push(
            //     new Response(Response.ERROR, err.message.toString())
            // ));
        } else {
            // console.error(`flash message info not set and errors is empty`);
        }
        req.flash('messages', messages);
        workflow.emit('redirect');
    });

    /**
     * Sent response via ajax
     */
    workflow.on('response', () =>
    {
        workflow.emit('preResponse');
        res.send(workflow.outcome);
    });

    /**
     * Render custom view file
     */
    workflow.on('render', view =>
    {
        workflow.emit('preResponse');
        res.render(view, workflow.outcome);
    });

    /**
     * Redirect route
     */
    workflow.on('redirect', (route = 'back') =>
    {
        res.redirect(route);
    });

    return workflow;
};