const del = require('del')
    , config = require('config')
    , uploadPath = config.get('uploadPath');

module.exports = (req, imagesArray = []) => {

    const FILE_UPLOADER_FIELD = 'fileuploader-list-files';
    const NEW_UPLOAD_PICTURES_REGEXP = new RegExp('0:\/');

    let outcome = {
        errors: { message: '' },
        data: [],
        hasErrors: function () {
            return !!this.errors.message;
        }
    };

    let imagesInput = req.body[FILE_UPLOADER_FIELD]; //modifies images from server JSON
    let images = [];

    images = imagesArray.slice(0);
    if (imagesInput) {
        let oldPictures = [];
        try {
            imagesInput = JSON.parse(imagesInput);
        } catch (e) {
            outcome.errors.push(e.message);
        }

        if (imagesInput.length) {
            //Find only previously saved images from JSON
            imagesInput.forEach(image => {
                if (!NEW_UPLOAD_PICTURES_REGEXP.test(image)) {
                    oldPictures.push(image);
                }
            });

            //Get difference between 2 image arrays
            images.forEach((image, index) => {
                if (!oldPictures.includes(image.file)) {
                    del.sync(`${uploadPath}/${image.name}`);
                    images.splice(index, 1);
                }
            });
            // if we have new uploaded files
            if (req.files.length) {
                req.files.forEach(file => images.push({
                    file: `/${uploadPath}/${file.filename}`,
                    name: file.filename,
                    size: file.size,
                    type: file.mimetype,
                }));
            }
        } else {
            images.length = 0;
        }
    }

    outcome.data = images.slice(0);

    return outcome;
};