module.exports = function (methods)
{
    return function (req, res, next)
    {
        if (methods.includes(req.method) || methods.includes(req.body._method)) {
            next();
        }
        else {
            const workflow = require('helpers/workflow')(req, res);
            res.status(404);
            workflow.pushError("Endpoint does not support '" + req.method + "' method");
            return workflow.emit('exception');
        }
    }
};