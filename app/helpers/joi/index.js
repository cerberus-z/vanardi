let Joi = require('joi');

module.exports = function (schema)
{
    return function (obj, callback)
    {
        Joi.validate(obj, schema, {stripUnknown: true}, callback);
    };
};