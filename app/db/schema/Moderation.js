'use strict';

let mongoose = require('mongoose');

const schema = new mongoose.Schema({
  moderated: [{
      action: { type: String },
      field: { type: mongoose.Schema.ObjectId },
      value: { type: String },
      approved: { type: Boolean },
      date : { type: Date },
      moderator_id: { type: mongoose.Schema.ObjectId }
    }]
});

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;
