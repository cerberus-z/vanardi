'use strict';

const mongoose = require('mongoose')
     , autoIncrement = require('mongoose-auto-increment');

const schema = new mongoose.Schema({
  full_name: { type: String },
  number: { type: String },
  create_at: { type: Date, default: Date.now },
  delivery_date: { type: String },
  delivery_time: { type: String },
  email: { type: String },
  phone: { type: String },
  address: { type: String },
  real_price: { type: String },
  total_price: { type: Number },
  promo_code: {type: String },
  items: [{
      ref_id: { type: String },
      type: { type: String },
      name: { type: String },
      avatar: { type: String },
      quantity: { type: Number },
      unit_cost: { type: Number },
      total_cost: { type: Number },
      date: {type: String},
      time: {type: String}
  }],
  status: { type: String, enum: ['pending', 'shipped', 'delivered', 'cancelled'] }
});

schema.plugin(autoIncrement.plugin, { model: 'Order', field: 'number', startAt: 1 });
schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;