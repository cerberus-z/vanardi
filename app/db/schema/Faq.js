'use strict';
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  question: { type: String },
  answer: { type: String }
});

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;
