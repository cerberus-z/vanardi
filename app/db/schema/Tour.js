'use strict';

const mongoose = require('mongoose')
    , uniqueValidator = require('mongoose-unique-validator');

const schema = new mongoose.Schema({
    date: { type: String},
    start_time: { type: String },
    end_time: { type: String },
    price: { type: Number, default: 0 },
    created_at: { type: Date, default: Date.now() }
});

schema.index({ date: 1, start_time: 1, end_time: 1 }, { unique: true });

// Plugins
schema.plugin(uniqueValidator, { message : 'Name must be unique.' });
schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;