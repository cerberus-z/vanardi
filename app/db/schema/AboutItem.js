'use strict';
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  title: { type: String },
  description: { type: String },
  date: { type: String },
  image: { type: String }
});

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;
