'use strict';
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    type: { type: String },
    amount: { type: Number }
});

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;