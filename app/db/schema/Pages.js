'use strict';

const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    type: { type: String },
    content: { type: mongoose.Schema.Types.Mixed, default: {} }
  },{ minimize: false } //allow to save empty objects
);

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));


module.exports = schema;
