'use strict';
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  title: { type: String },
  images: { type: String },
});

schema.plugin(require('mongoose-beautiful-unique-validation'));
module.exports = schema;
