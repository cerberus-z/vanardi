'use strict';

const mongoose = require('mongoose');
const schema = new mongoose.Schema({
    name: { type: String },
    address: { type: String },
    phone: { type: String }
});

schema.plugin(require('./plugins/pagedFind'));

module.exports = schema;