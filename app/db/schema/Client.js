'use strict';
const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  create_at: { type: Date, default: Date.now },
  role: {
    type: String,
    required: true,
    enum: ['subscriber','club_member']
  },
  birth_date: { type: String },
  email: { type: String, required: 'Email not be empty', unique: 'This email address taken'},
  address: { type: String },
  phone: { type: String },
  promo_code: { type: String },
  status: { type: String, enum: ['pending', 'confirmed'] }
});

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;
