'use strict';

const mongoose = require('mongoose');

const medalSchema = new mongoose.Schema({
    name: { type: String },
    date: { type: String },
    image: { type: String }
});

const feedbackSchema = new mongoose.Schema({
    full_name: { type: String },
    description: { type: String },
    date: { type: String },
    rate: { type: String },
    confirmed: { type: Boolean }
});

const schema = new mongoose.Schema({
    name: { type: String },
    avatar: { type: String },
    description: { type: String },
    in_stock: { type: Number },
    price: { type: Number },
    discount: { type: Number, default: 0 },
    gallery: [],
    tag: [ {type: String} ],
    alcohol_lvl: { type: Number },
    barrel_aged: { type: Boolean },
    featured: { type: Boolean },
    pairings: [{ type: mongoose.Schema.ObjectId, ref: 'Pairings' }],
    medals: [medalSchema],
    feedback: [feedbackSchema]
});
schema.plugin(require('./plugins/pagedFind'));

module.exports = schema;
