'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

let schema = new mongoose.Schema({
    name: { type: String, required: true },
    create_at: { type: Date, default: Date.now },
    role: {
        type: String,
        required: true,
        enum: ['admin', 'moderator']
    },
    password: { type: String, required: true, select: false},
    email: { type: String, required: 'Email not be empty', unique: 'This email address taken.'},
    phone: { type: String },
});

schema.pre('save', function (next) {
    let user = this;
    if (!user.isModified('password')) {
        return next();
    }
	bcrypt.hash(user.password, null, null, function(err, hash){
		if (err) {
            return next(err);
        }
		user.password = hash;
		next();
	});
});

schema.methods.comparePasswords = function(password) {
    let user = this;
    return bcrypt.compareSync(password, user.password);
};

schema.plugin(require('./plugins/pagedFind'));
schema.plugin(require('mongoose-beautiful-unique-validation'));

module.exports = schema;
