const mongoose = require('mongoose')
    , autoIncrement = require('mongoose-auto-increment')
    , config = require('config');

mongoose.set('debug', config.get('mongoose:debug'));
mongoose.Promise = global.Promise;

const db = mongoose.createConnection(config.get('mongoose:db:uri'), config.get('mongoose:db:options'));

autoIncrement.initialize(db);
db.on('connected', () => console.log("mongoose connected"));
// db.once('open', function () {});

db.on('error', function (err) {
    console.log(err);
    process.exit(0);
});

db.on('disconnected', function () {
    console.log("mongoose disconnect");
});

db.model("Event", require('./schema/Event.js'), config.get('mongoose:collections:event'));
db.model("Tour", require('./schema/Tour.js'), config.get('mongoose:collections:tour'));
db.model("Moderation", require('./schema/Moderation.js'), config.get('mongoose:collections:moderation'));
db.model("Order", require('./schema/Order.js'), config.get('mongoose:collections:orders'));
db.model("Pages", require('./schema/Pages.js'), config.get('mongoose:collections:pages'));
db.model("AboutItem", require('./schema/AboutItem.js'), config.get('mongoose:collections:about-item'));
db.model("Pairings", require('./schema/Pairings.js'), config.get('mongoose:collections:pairings'));
db.model("News", require('./schema/News.js'), config.get('mongoose:collections:news'));
db.model("Products", require('./schema/Products.js'), config.get('mongoose:collections:products'));
db.model("Faq", require('./schema/Faq.js'), config.get('mongoose:collections:faq'));
db.model("Client", require('./schema/Client.js'), config.get('mongoose:collections:clients'));
db.model("Users", require('./schema/Users.js'), config.get('mongoose:collections:users'));
db.model("Stores", require('./schema/Stores.js'), config.get('mongoose:collections:stores'));
db.model("Settings", require('./schema/Settings.js'), config.get('mongoose:collections:settings'));

module.exports = db;
