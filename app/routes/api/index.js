const express = require('express')
    , router = express.Router();

router.use('/products', require('./products'));
router.use('/club-membership', require('./club-membership'));
router.use('/orders', require('./orders'));
router.use('/pages', require('./pages'));
router.use('/news', require('./news'));
router.use('/events', require('./events'));
router.use('/tours', require('./tours'));
router.use('/settings', require('./common'));

module.exports = router;