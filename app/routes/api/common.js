const express = require('express')
    , router = express.Router()
    , CommonController = require('controllers/common')
    , supportedMethods = require('helpers/api/supportedMethods.js');

router
    .route('/')
    .all(supportedMethods(['GET']))
    .get(CommonController.get);


module.exports = router;