const express = require('express')
    , router = express.Router()
    , OrderController = require('controllers/orders')
    , supportedMethods = require('helpers/api/supportedMethods.js');

router
    .route('/')
    .all(supportedMethods(['POST']))
    .post(OrderController.create);

router
    .route('/checkPromo')
    .all(supportedMethods(['POST']))
    .post(OrderController.checkPromoCode);

module.exports = router;