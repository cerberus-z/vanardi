const express = require('express')
    , router = express.Router()
    , ClubMemberController = require('controllers/club-members')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , async = require('async');

router
    .route('/')
    .all(supportedMethods(['POST']))
    .post(ClubMemberController.create);

module.exports = router;