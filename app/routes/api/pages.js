const express = require('express')
    , router = express.Router()
    , AboutItem = require('controllers/pages/about-item')
    , ClubMembership = require('controllers/pages/club-membership')
    , Page = require('db').model('Pages')
    , Product = require('db').model('Products')
    , Faq = require('db').model('Faq')
    , Tour = require('db').model('Tour')
    , Event = require('db').model('Event')
    , Stores = require('db').model('Stores')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , Client = require('db').model('Client')
    , validator = require('controllers/subscribers/validators')
    , async = require('async')
    , moment = require('moment');

router
    .route('/home')
    .all(supportedMethods(['GET']))
    .get(getHome);

router
    .route('/contact')
    .all(supportedMethods(['GET']))
    .get(getContactAndFaq);

router
    .route('/contact/subscribe')
    .all(supportedMethods(['POST']))
    .post(subscribeToNewsletter);

router
    .route('/our-wines')
    .all(supportedMethods(['GET']))
    .get(getWines);

router
    .route('/our-wines/:id')
    .all(supportedMethods(['GET']))
    .get(getWine);

router
    .route('/wine-tour')
    .all(supportedMethods(['GET']))
    .get(getTour);

router
    .route('/about')
    .all(supportedMethods(['GET']))
    .get(AboutItem.get);

router
    .route('/club-membership')
    .all(supportedMethods(['GET']))
    .get(ClubMembership.get);


function getHome (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    async.parallel({
        content: (callback) => {
            Page.findOne({ type: 'home' }, (err, page) => {
                if (err || !page) {
                    return workflow.emit('exception', err);
                }
                callback(null, page.content);
            })
        },
        events: (callback) => {
            Event
                .find({})
                .exec((err, events) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }

                    callback(null, events);
                });
        },
         wines: (callback) => {
             Product
                 .find({ featured: true})
                 .exec((err, products) => {
                     if (err) {
                         return workflow.emit('exception', err);
                     }

                     callback(null, products);
                 });
         },
    }, (err, results) => {
        if (err) {
            return workflow.emit('exception', err);
        }

        workflow.outcome.content = results.content;
        workflow.outcome.events = results.events;
        workflow.outcome.wines = results.wines;

        workflow.emit('response');
    });
};

function subscribeToNewsletter (req, res) {
    let workflow = require('helpers/workflow')(req, res);
    let requestObject;

    workflow.on('validate', () => {
        validator.ioValidator(req)(req.body, (err, validRequestObject) => {
            if (err) {
                return workflow.validationError(err);
            }

            if (Object.keys(validRequestObject).length == 0) {
                workflow.pushError('Request body empty');
                return workflow.emit('exception');
            }
            requestObject = validRequestObject;
            return workflow.emit('create');
        })
    });

    workflow.on('create', () => {
        requestObject.role = 'subscriber';
        let client = new Client(requestObject);
        client.save((err, newClient) =>  {
            if (err || !newClient) {
                return workflow.validationError(err);
            }
            workflow.emit('response');
        });
    });

    workflow.emit('validate');
}

function getWines (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    async.parallel({
        content: (callback) => {
            Page.findOne({ type: 'our-wines' }, (err, page) => {
                if (err || !page) {
                    return workflow.emit('exception', err);
                }
                callback(null, page.content);
            })
        },
        wines: (callback) => {
            Product
                .find({})
                .exec((err, products) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }

                    callback(null, products);
                });
        }
    }, (err, results) => {
        if (err) {
            return workflow.emit('exception', err);
        }

        workflow.outcome.content = results.content;
        workflow.outcome.wines = results.wines;

        workflow.emit('response');
    });
};

function getWine (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    Product
        .findOne({
            _id: req.params.id,
        })
        .populate({ path: 'pairings' })
        .exec((err, product) => {
            if (err) {
                return workflow.emit('exception', err);
            }

            product.feedback = product.feedback.filter(feedback => feedback.confirmed );
            workflow.outcome.wine = product;

            workflow.emit('response');
        });
}

function getContactAndFaq (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    async.parallel({
        contact: (callback) => {
            Page.findOne({ type: 'contact' }, (err, page) => {
                if (err || !page) {
                    return workflow.emit('exception', err);
                }
                callback(null, page.content);
            })
        },
        faq: (callback) => {
            Faq
                .find({})
                .exec((err, faqs) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }

                    callback(null, faqs);
                });
        },
        stores: (callback) => {
            Stores
                .find({})
                .exec((err, stores) => {
                    if (err) {
                        return workflow.emit('exception', err);
                    }
                    callback(null, stores);
                });
        }
    }, (err, results) => {
        if (err) {
            return workflow.emit('exception', err);
        }

        workflow.outcome.contact = results.contact;
        workflow.outcome.faq = results.faq;
        workflow.outcome.stores = results.stores;

        workflow.emit('response');
    });
};

function getTour (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    async.parallel({
        content: (callback) => {
            Page.findOne({ type: 'wine-tours' }, (err, page) => {
                if (err || !page) {
                    return workflow.emit('exception', err);
                }
                callback(null, page.content);
            })
        },
        tours: (callback) => {
            Tour
                .find({})
                .exec((err, tours) => {
                    callback(err, tours);
                });
        }
    }, (err, results) => {
        if (err) {
            return workflow.emit('exception', err);
        }
        workflow.outcome.content = results.content;
        workflow.outcome.tours = results.tours;

        workflow.emit('response');
    });
}

module.exports = router;