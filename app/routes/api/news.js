const express = require('express')
    , router = express.Router()
    , NewsController = require('controllers/news')
    , supportedMethods = require('helpers/api/supportedMethods.js');

router
    .route('/')
    .all(supportedMethods(['GET']))
    .get(NewsController.get);

router
    .route('/:id')
    .all(supportedMethods(['GET']))
    .get(NewsController.show);


module.exports = router;