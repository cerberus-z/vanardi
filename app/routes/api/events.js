const express = require('express')
    , router = express.Router()
    , EventsController = require('controllers/events')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , async = require('async');

router
    .route('/')
    .all(supportedMethods(['GET']))
    .get(EventsController.get);

router
    .route('/:id')
    .all(supportedMethods(['GET']))
    .get(EventsController.show);


module.exports = router;