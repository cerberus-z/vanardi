const express = require('express')
    , router = express.Router()
    , ProductController = require('controllers/products')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , async = require('async');

router
    .route('/:id')
    .all(supportedMethods(['POST']))
    .post(ProductController.addFeedback);

module.exports = router;