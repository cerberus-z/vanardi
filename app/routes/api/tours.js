const express = require('express')
    , router = express.Router()
    , TourController = require('controllers/tours')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , async = require('async');

router
    .route('/')
    .all(supportedMethods(['GET']))
    .get(TourController.get);

router
    .route('/:id')
    .all(supportedMethods(['GET']))
    .get(TourController.show);


module.exports = router;