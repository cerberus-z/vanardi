module.exports = app => {
    //Admin side routing
    app.use('/admin', require('./admin'));
    app.use('/auth', require('./auth'));
    app.use('/api', require('./api'));
};