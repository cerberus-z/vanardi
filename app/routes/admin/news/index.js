const express = require('express')
    , NewsController = require('controllers/news')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(NewsController.get)
    .post(NewsController.store);

router
    .route('/create')
    .all(isAuthenticated, supportedMethods(['GET']))
    .get(NewsController.create);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'GET', 'DELETE']))
    .get(NewsController.edit)
    .put(NewsController.update)
    .delete(NewsController.remove);

module.exports = router;