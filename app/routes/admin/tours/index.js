const express = require('express')
    , TourController = require('controllers/tours')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(TourController.get)
    .post(TourController.store);

router
    .route('/private-tour-price')
    .all(isAuthenticated, supportedMethods(['POST']))
    .post(TourController.privateTourPrice);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(TourController.update)
    .delete(TourController.remove);


module.exports = router;