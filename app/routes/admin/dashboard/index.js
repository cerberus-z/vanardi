const express = require('express')
    , CommonController = require('controllers/common')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET']))
    .get(CommonController.dashboard);

module.exports = router;