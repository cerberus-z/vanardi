const express = require('express')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , ProductController = require('controllers/products')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET']))
    .get(ProductController.get);

router
    .route('/create')
    .all(isAuthenticated, supportedMethods(['POST', 'GET']))
    .get(ProductController.create)
    .post(ProductController.store);

router
    .route('/medals/:id?')
    .all(isAuthenticated, supportedMethods(['POST', 'PUT',  'DELETE']))
    .put(ProductController.updateMedal)
    .post(ProductController.storeMedal)
    .delete(ProductController.removeMedal);

router
    .route('/:prodId/feedback')
    .all(supportedMethods(['PUT']))
    .put(ProductController.editFeedback);

router
    .route('/:id/:gallery?')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT', 'DELETE']))
    .get(ProductController.edit)
    .put(ProductController.update)
    .delete(ProductController.remove);

module.exports = router;