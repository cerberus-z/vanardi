const express = require('express')
    , router = express.Router()
    , CommonController = require('controllers/common')
    , supportedMethods = require('helpers/api/supportedMethods.js');

router
    .route('/badges')
    .all(supportedMethods(['GET']))
    .get(CommonController.getBadges);


module.exports = router;