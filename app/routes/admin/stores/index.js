const express = require('express')
    , StoreController = require('controllers/stores')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(StoreController.get)
    .post(StoreController.store);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(StoreController.update)
    .delete(StoreController.remove);

module.exports = router;