const express = require('express')
    , SubscriberController = require('controllers/subscribers')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(SubscriberController.get)
    .post(SubscriberController.store);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(SubscriberController.update)
    .delete(SubscriberController.remove);

module.exports = router;