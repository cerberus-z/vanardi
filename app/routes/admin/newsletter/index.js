const express = require('express')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , Response = require('helpers/workflow/Response.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET']))
    .get(index);

function index (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    workflow.emit('render', 'admin/newsletter');
}

module.exports = router;