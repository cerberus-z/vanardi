const express = require('express')
    , router = express.Router()
    , authGuard = require('middleware/authorized');

router.use('/', authGuard.isAuthenticated, require('./dashboard'));
router.use('/orders', authGuard.isAuthenticated, require('./orders'));
router.use('/newsletter', authGuard.isAuthenticated, require('./newsletter'));
router.use('/subscribers', authGuard.isAuthenticated, require('./subscribers'));
router.use('/members', authGuard.isAuthenticated, require('./club-members'));
router.use('/moderators', authGuard.isAuthenticated, require('./moderators'));
router.use('/products', authGuard.isAuthenticated, require('./products'));
router.use('/faq', authGuard.isAuthenticated, require('./faq'));
router.use('/pages', authGuard.isAuthenticated, require('./pages'));
router.use('/pairings', authGuard.isAuthenticated, require('./pairings'));
router.use('/events', authGuard.isAuthenticated, require('./events'));
router.use('/news', authGuard.isAuthenticated, require('./news'));
router.use('/tours', authGuard.isAuthenticated, require('./tours'));
router.use('/common', authGuard.isAuthenticated, require('./common'));
router.use('/stores', authGuard.isAuthenticated, require('./stores'));

module.exports = router;
