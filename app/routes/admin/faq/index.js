const express = require('express')
    , router = express.Router()
    , FaqController = require('controllers/faq')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(FaqController.get)
    .post(FaqController.store);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(FaqController.update)
    .delete(FaqController.remove);

module.exports = router;