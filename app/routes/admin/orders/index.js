const express = require('express')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , OrderController = require('controllers/orders')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST', 'DELETE']))
    .get(OrderController.get)
    .post(OrderController.create)
    .delete(OrderController.remove);

router
    .route('/:id/:itemId?')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT', 'DELETE']))
    .get(OrderController.edit)
    .put(OrderController.update)
    .delete(OrderController.removeItem);


module.exports = router;