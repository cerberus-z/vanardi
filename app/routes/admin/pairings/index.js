const express = require('express')
    , router = express.Router()
    , PairingsController = require('controllers/pairings')
    , isAuthenticated = require('middleware/authorized').isAuthenticated    
    , supportedMethods = require('helpers/api/supportedMethods.js');

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(PairingsController.get)
    .post(PairingsController.store);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['DELETE']))
    .delete(PairingsController.remove);

module.exports = router;