const express = require('express')
    , EventController = require('controllers/events')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(EventController.get)
    .post(EventController.store);

router
    .route('/create')
    .all(isAuthenticated, supportedMethods(['GET']))
    .get(EventController.create);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT', 'DELETE']))
    .get(EventController.edit)
    .put(EventController.update)
    .delete(EventController.remove);

module.exports = router;