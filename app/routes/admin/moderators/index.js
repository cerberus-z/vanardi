const express = require('express')
    , ModeratorController = require('controllers/moderators')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , Response = require('helpers/workflow/Response.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(ModeratorController.get)
    .post(ModeratorController.store);

router
    .route('/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(ModeratorController.update)
    .delete(ModeratorController.remove);

module.exports = router;