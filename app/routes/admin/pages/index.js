const express = require('express')
    , router = express.Router()
    , AboutItem = require('controllers/pages/about-item')
    , Contact = require('controllers/pages/contact')
    , ClubMembership = require('controllers/pages/club-membership')
    , WineTour = require('controllers/pages/wine-tour')
    , Home = require('controllers/pages/home')
    , OurWines = require('controllers/pages/our-wines')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/home')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT']))
    .get(Home.get)
    .put(Home.update);

router
    .route('/our-wines')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT']))
    .get(isAuthenticated, OurWines.get)
    .put(isAuthenticated, OurWines.update);

router
    .route('/wine-tours')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT']))
    .get(WineTour.get)
    .put(WineTour.update);

router
    .route('/club-membership')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT']))
    .get(ClubMembership.get)
    .put(ClubMembership.update);

router
    .route('/about')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(AboutItem.get)
    .post(AboutItem.create);

router
    .route('/about/:id')
    .all(isAuthenticated, supportedMethods(['PUT', 'DELETE']))
    .put(AboutItem.update)
    .delete(AboutItem.remove);

router
    .route('/contact')
    .all(isAuthenticated, supportedMethods(['GET', 'PUT']))
    .get(Contact.get)
    .put(Contact.update);

module.exports = router;