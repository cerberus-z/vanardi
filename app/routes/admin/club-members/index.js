const express = require('express')
    , router = express.Router()
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , ClubMemberController = require('controllers/club-members')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(isAuthenticated, supportedMethods(['GET', 'POST']))
    .get(ClubMemberController.get)
    .post(ClubMemberController.create);

router
    .route('/:id')
    .all(supportedMethods(['PUT', 'DELETE']))
    .put(ClubMemberController.update)
    .delete(ClubMemberController.remove);

module.exports = router;