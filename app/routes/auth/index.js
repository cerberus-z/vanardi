const express = require('express')
    , router = express.Router()
    , config = require('config')
    , jwt = require('jsonwebtoken')
    , User = require('db').model('Users')
    , Response = require('helpers/workflow/Response.js')
    , supportedMethods = require('helpers/api/supportedMethods.js')
    , isAuthenticated = require('middleware/authorized').isAuthenticated;

router
    .route('/')
    .all(supportedMethods(['GET']))
    .get(index);

router
    .route('/login')
    .all(supportedMethods(['POST']))
    .post(login);

router
    .route('/logout')
    .get(supportedMethods(['GET']))
    .get(logout);

function index (req, res) {
    res.render('auth/login');
}

function login (req, res) {
    User
        .findOne({
            email: req.body.email
        })
        .select('email password name')
        .exec((err, user) => {
            let workflow = require('helpers/workflow')(req, res);

            if (err) {
                return workflow.emit('exception', err);
            }

            if (!user) {
                workflow.pushError(`Wrong username or password`);
                return workflow.emit('exception');
            }

            let validPassword = user.comparePasswords(req.body.password);

            if (!validPassword) {
                workflow.pushError(`Wrong username or password`);
                return workflow.emit('exception');
            }

            let signedToken = jwt.sign(
                { name: user.name, email: user.email },
                config.get('jwt:superSecret'),
                { expiresIn: config.get('jwt:expiresIn') }
            );

            res.cookie('token', signedToken, config.get('jwt:expiresIn'));
            return workflow.emit('redirect', '/admin');
        });
}

function logout (req, res) {
    let workflow = require('helpers/workflow')(req, res);

    res.clearCookie('token');
    
    return workflow.emit('redirect', '/admin');
}

module.exports = router;
