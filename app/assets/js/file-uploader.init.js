/**
* Theme: Ubold Admin Template
* Author: Coderthemes
* Module/App: Main Js
*/


!function($) {
    "use strict";

    $(function() {
        $('body').on('change', '.filestyle', function() {
            var preview = $(this).parents().eq(2).find('.thumb-img')
                , linkTag = $(preview).closest('.image-popup')
                , file    = this.files[0]
                , reader  = new FileReader();

            reader.addEventListener("load", function () {
                preview.attr('src', reader.result);
                linkTag.attr('href', reader.result);
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        });
    });

}( jQuery );

