JQFiler = {
    init: init
};

function init(files) {
    $('input[name="files"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: ' ',
        theme: 'thumbnails',
        enableApi: true,
        addMore: true,
        files: files || [],
        thumbnails: {
            removeConfirmation: true,
            box: '<div class="fileuploader-items">' +
                '<ul class="fileuploader-items-list">' +
                '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
                '</ul>' +
                '</div>',
            item: '<li class="fileuploader-item">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="thumbnail-holder">${image}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                '</div>' +
                '</div>' +
                '</li>',
            item2: '<li class="fileuploader-item">' +
                '<div class="fileuploader-item-inner">' +
                '<div class="thumbnail-holder">${image}</div>' +
                '<div class="actions-holder">' +
                '<a class="fileuploader-action fileuploader-action-remove" title="Remove"><i class="remove"></i></a>' +
                '</div>' +
                '</div>' +
                '</li>',
            _selectors: {
                list: '.fileuploader-items-list',
                item: '.fileuploader-item',
                start: '.fileuploader-action-start',
                retry: '.fileuploader-action-retry',
                remove: '.fileuploader-action-remove'
            },
            startImageRenderer: true,
            onItemShow: function(item, listEl) {
                var plusInput = listEl.find('.fileuploader-thumbnails-input');

                plusInput.insertAfter(item.html);

                if (item.format == 'image') {
                    item.html.find('.fileuploader-item-icon').hide();
                    // item.html.data('index', item.index);
                }
            },
            onItemRemove: function(itemEl, listEl, parentEl, newInputEl, inputEl) {
                itemEl.children().animate({'opacity': 0}, 200, function() {
                    setTimeout(function() {
                        itemEl.slideUp(200, function() {
                            itemEl.remove();
                        });
                    }, 100);
                });
            },
        },
        afterRender: function(listEl, parentEl, newInputEl, inputEl) {
            var plusInput = listEl.find('.fileuploader-thumbnails-input'),
                api = $.fileuploader.getInstance(inputEl.get(0));

            plusInput.on('click', function() {
                api.open();
            });
        },
        /*
         // while using upload option, please set
         // startImageRenderer: false
         // for a better effect
         upload: {
         url: './php/upload_file.php',
         data: null,
         type: 'POST',
         enctype: 'multipart/form-data',
         start: true,
         synchron: true,
         beforeSend: null,
         onSuccess: function(data, item) {
         setTimeout(function() {
         item.html.find('.progress-holder').hide();
         item.renderImage();
         }, 400);
         },
         onError: function(item) {
         item.html.find('.progress-holder').hide();
         item.html.find('.fileuploader-item-icon i').text('Failed!');

         setTimeout(function() {
         item.remove();
         }, 1500);
         },
         onProgress: function(data, item) {
         var progressBar = item.html.find('.progress-holder');

         if(progressBar.length > 0) {
         progressBar.show();
         progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
         }
         }
         },
         dragDrop: {
         container: '.fileuploader-thumbnails-input'
         },
         onRemove: function(item) {
         $.post('php/upload_remove.php', {
         file: item.name
         });
         },
         */
    });
}