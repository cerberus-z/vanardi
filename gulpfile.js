'use strict';
const paths = {
    src: 'src/',
    dist: 'dist/'
};
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    minifyCSS = require('gulp-clean-css'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate'),
    watch = require('gulp-watch'),
    usemin = require('gulp-usemin'),
    htmlmin = require('gulp-htmlmin'),
    rename = require('gulp-rename'),
    templateCache = require('gulp-angular-templatecache'),
    clean = require('gulp-clean'),
    path = require('path');

gulp.task('css', function() {
    return gulp.src('src/assets/css/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('src/assets/css'))
});

gulp.task('less-css', function() {
    return gulp.src(['app/assets/less/icons.less','app/assets/less/pages.less'])
        .pipe(less())
        .pipe(gulp.dest('app/assets/css'))
});


gulp.task('clean', function () {
    return gulp.src("dist/*", { read: false }).pipe(clean());
});

gulp.task('copy', function () {
    return gulp
        .src([path.join(paths.src, 'assets/img/*'), path.join(paths.src, 'assets/img/**/*')])
        .pipe(gulp.dest(path.join(paths.dist,'img')));
});

gulp.task('copy-fonts', function () {
    return gulp
        .src([path.join(paths.src, 'assets/fonts/*'), path.join(paths.src, 'assets/fonts/**/*')])
        .pipe(gulp.dest(path.join(paths.dist,'fonts')));
});

gulp.task('angular', function() {
    return gulp
        .src([
            path.join(paths.src, 'app/*.js'),
            path.join(paths.src, 'app/**/*.js')
        ])
        .pipe(ngAnnotate())
        .pipe(concat('app.js'))
        // .pipe(uglify())
        .pipe(gulp.dest(path.join(paths.dist, 'js/'))); 
});

gulp.task('ng-template', function() {
    return gulp
        .src([
            path.join(paths.src, 'app/views/*.html'),
            path.join(paths.src, 'app/views/**/*.html')
        ])
        .pipe(templateCache({
            root: 'app/views/'
        }))
        .pipe(gulp.dest(path.join(paths.dist, 'js')));
});

gulp.task('usemin', function() {
    return gulp.src(path.join(paths.src, 'index.html'))
        .pipe(usemin({
            html: [ htmlmin({ collapseWhitespace: true }) ],
            css: [
                sass().on('error', sass.logError),
                minifyCSS(),
                rename({ suffix: '.min' })
            ]
        }))
        .pipe(gulp.dest(paths.dist))
});

gulp.task('watch-public', function() {
    gulp.watch(['src/**/*.html','src/*.html'], gulp.parallel('ng-template'));
    gulp.watch(['src/assets/css/*.scss','src/assets/css/**/*.scss'], gulp.series('css', 'usemin'));
    gulp.watch(['app/assets/less/*.less','app/assets/less/icons/**/*.less'], gulp.parallel('less-css'));
    gulp.watch(['src/app/*.js', 'src/app/**/*.js'], gulp.parallel('angular', 'ng-template', 'usemin'));
});

gulp.task('default', gulp.series('clean', 'copy', 'copy-fonts', 'css', 'angular', 'ng-template', 'usemin', 'watch-public'));
